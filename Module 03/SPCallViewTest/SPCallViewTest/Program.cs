﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace SPCallViewTest
{
    class Program
    {
        static string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=AdventureWorks2012;Integrated Security=True";
        static void Main(string[] args)
        {
            //AddRole("rolename6", 'I');
            //Console.WriteLine("===============================================");

            //AddUser("new user from VS");

            //Console.WriteLine("===============================================");
            //AddUserToRole(2, 2);

            //Console.WriteLine("===============================================");
            //DateTime time = DateTime.Now;
            //AddLog("insert from VS", "ROLES", 1, "log_info", time);

            //Console.WriteLine("===============================================");
            //Console.WriteLine("User rules count");
            //int cntUserRule = udfCountUserRule('D');
            //Console.WriteLine(cntUserRule);

            //Console.WriteLine("===============================================");
            //Console.WriteLine("User roles");
            //var roles = udfShowUserRule(1);
            //foreach (var role in roles)
            //{
            //    Console.WriteLine(role);
            //}

            //Console.WriteLine("===============================================");
            //Console.WriteLine("Role users");
            //var users = udf_show_user_for_rule('D');
            //foreach (var user in users)
            //{
            //    Console.WriteLine(user);
            //}

            Console.WriteLine("===============================================");
            TestDSNOdbcConnection();
            Console.ReadKey();
        }

        private static void AddRole(string name, char role)
        {
            string sqlExpression = "ADD_ROLE";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter nameParam = new SqlParameter
                {
                    ParameterName = "@name",
                    Value = name
                };
                command.Parameters.Add(nameParam);
                SqlParameter roleParam = new SqlParameter
                {
                    ParameterName = "@role",
                    Value = role
                };
                command.Parameters.Add(roleParam);

                SqlParameter idParam = new SqlParameter
                {
                    ParameterName = "@id",
                    SqlDbType = SqlDbType.Int
                };
                idParam.Direction = ParameterDirection.Output;
                command.Parameters.Add(idParam);
                command.ExecuteNonQuery();
                Console.WriteLine("Id добавленного объекта: {0}", command.Parameters["@id"].Value);
            }
        }

        private static void AddUser(string name)
        {
            string sqlExpression = "ADD_USER";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter nameParam = new SqlParameter
                {
                    ParameterName = "@name",
                    Value = name
                };
                command.Parameters.Add(nameParam);

                SqlParameter idParam = new SqlParameter
                {
                    ParameterName = "@id",
                    SqlDbType = SqlDbType.Int
                };
                idParam.Direction = ParameterDirection.Output;
                command.Parameters.Add(idParam);
                command.ExecuteNonQuery();
                Console.WriteLine("Id добавленного объекта: {0}", command.Parameters["@id"].Value);
            }
        }

        private static void AddUserToRole(int id_user, int id_role)
        {
            string sqlExpression = "ADD_USER_TO_ROLE";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter id_userParam = new SqlParameter
                {
                    ParameterName = "@id_user",
                    Value = id_user
                };
                command.Parameters.Add(id_userParam);

                SqlParameter id_roleParam = new SqlParameter
                {
                    ParameterName = "@id_role",
                    Value = id_role
                };
                command.Parameters.Add(id_roleParam);

                SqlParameter idParam = new SqlParameter
                {
                    ParameterName = "@id",
                    SqlDbType = SqlDbType.Int
                };
                idParam.Direction = ParameterDirection.Output;
                command.Parameters.Add(idParam);
                command.ExecuteNonQuery();
                Console.WriteLine("Id добавленного объекта: {0}", command.Parameters["@id"].Value);
            }
        }

        private static void AddLog(string action, string table_name, int id_row, string log_info, DateTime time)
        {
            string sqlExpression = "ADD_LOG";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                //@action nchar(100),@table_name nvarchar(100),@id_row int,@log_info nvarchar(100),@log_time
                SqlParameter actionParam = new SqlParameter
                {
                    ParameterName = "@action",
                    Value = action
                };
                command.Parameters.Add(actionParam);

                SqlParameter table_nameParam = new SqlParameter
                {
                    ParameterName = "@table_name",
                    Value = table_name
                };
                command.Parameters.Add(table_nameParam);

                SqlParameter id_rowParam = new SqlParameter
                {
                    ParameterName = "@id_row",
                    Value = id_row
                };
                command.Parameters.Add(id_rowParam);

                SqlParameter log_infoParam = new SqlParameter
                {
                    ParameterName = "@log_info",
                    Value = log_info
                };
                command.Parameters.Add(log_infoParam);

                SqlParameter log_timeParam = new SqlParameter
                {
                    ParameterName = "@log_time",
                    Value = time
                };
                command.Parameters.Add(log_timeParam);

                SqlParameter idParam = new SqlParameter
                {
                    ParameterName = "@id",
                    SqlDbType = SqlDbType.Int
                };
                idParam.Direction = ParameterDirection.Output;
                command.Parameters.Add(idParam);
                command.ExecuteNonQuery();
                Console.WriteLine("Id добавленного объекта: {0}", command.Parameters["@id"].Value);
            }
        }

        private static int udfCountUserRule(char permission)
        {
            string sqlExpression = String.Format("SELECT dbo.udf_count_user_rule('{0}') as USERS_RULE", permission);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = new SqlCommand(sqlExpression, connection);
                var res = command.ExecuteScalar();
                return int.Parse(res.ToString());
            }
        }

        private static List<string> udfShowUserRule(int id)
        {
            var roles = new List<string>();

            string sqlExpression = String.Format("SELECT * FROM udf_show_user_rule ({0}) as USER_RULE", id);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = new SqlCommand(sqlExpression, connection);
                using (var sqlDataReader = command.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            string role = sqlDataReader.GetString(0);
                            roles.Add(role);
                        }
                    }
                }
            }

            return roles;
        }

        private static List<string> udf_show_user_for_rule(char permission)
        {
            var users = new List<string>();

            string sqlExpression = String.Format("SELECT * FROM udf_show_user_for_rule ('{0}') as ROLE_USERS", permission);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                var command = new SqlCommand(sqlExpression, connection);
                using (var sqlDataReader = command.ExecuteReader())
                {
                    if (sqlDataReader.HasRows)
                    {
                        while (sqlDataReader.Read())
                        {
                            string role = sqlDataReader.GetString(0);
                            users.Add(role);
                        }
                    }
                }
            }

            return users;
        }

        private static void TestDSNOdbcConnection()
        {
            OdbcConnection DbConnection = new OdbcConnection("DSN=MY_DB");
            DbConnection.Open();
            OdbcCommand DbCommand = DbConnection.CreateCommand();
            DbCommand.CommandText = "select Person.Person.BusinessEntityID,Person.Person.FirstName from Person.Person";
            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            int fCount = DbReader.FieldCount;
            Console.Write(":");
            for (int i = 0; i < fCount; i++)
            {
                String fName = DbReader.GetName(i);
                Console.Write(fName + ":");
            }
            Console.WriteLine();
            while (DbReader.Read())
            {
                Console.Write(":");
                for (int i = 0; i < fCount; i++)
                {
                    String col = DbReader.GetString(i);
                    Console.Write(col + ":");
                }
                Console.WriteLine();
            }

            DbReader.Close();
            DbCommand.Dispose();
            DbConnection.Close();
            Console.WriteLine("Press any...");
            Console.ReadKey();
        }

    }
}



