﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    public class DBaccess
    {
        public static string connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=DBforTASK;Integrated Security=True";
        public static DataView GetDepartments(DataSet data)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "select * from DEPARTMENTS";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                    adapter.Fill(data, "DEPARTMENTS");
                    DataView dv = data.Tables["DEPARTMENTS"].DefaultView;
                    return dv;
                }
            }
        }

        public static DataView GetProducts(DataSet data)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "select * from PRODUCTS";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                    adapter.Fill(data, "PRODUCTS");
                    DataView dv = data.Tables["PRODUCTS"].DefaultView;
                    return dv;
                }
            }
        }

        public static DataView GetEmployees(DataSet data)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "select * from EMPLOYEES";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                    adapter.Fill(data, "EMPLOYEES");
                    DataView dv = data.Tables["EMPLOYEES"].DefaultView;
                    return dv;
                }
            }
        }

        public static DataView GetBanks(DataSet data)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "select * from BANKS";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                    adapter.Fill(data, "BANKS");
                    DataView dv = data.Tables["BANKS"].DefaultView;
                    return dv;
                }
            }
        }

        public static DataView GetClients(DataSet data)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                string sql = "select * from CLIENTS";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
                    adapter.Fill(data, "CLIENTS");
                    DataView dv = data.Tables["CLIENTS"].DefaultView;
                    return dv;
                }
            }
        }
        public static void InsertDepartment(DataSet data)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();

                string sql = @"
                INSERT into DEPARTMENTS
                (
                   DEPARTMENTS_NAME,
                   REGION_INFO
                )
                values
                (
                  @DEPARTMENTS_NAME,
                  @REGION_INFO
                )";

                SqlCommand sqlCom = new SqlCommand(sql, conn);

                SqlParameter param1 = sqlCom.Parameters.Add("@DEPARTMENTS_NAME", SqlDbType.Char, 4, "DEPARTMENTS_NAME");
                param1.SourceVersion = DataRowVersion.Current;
               
                SqlParameter param2 = sqlCom.Parameters.Add("@REGION_INFO", SqlDbType.NChar, 40, "REGION_INFO");
                param2.SourceVersion = DataRowVersion.Current;

                adapter.InsertCommand = sqlCom;
                adapter.Update(data, "DEPARTMENTS");
            }
        }     
        public static void UpdateDepartment(int id, string name,string region)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string sql = String.Format($"UPDATE DEPARTMENTS SET DEPARTMENTS_NAME ='{name}',REGION_INFO='{region}' WHERE DEPARTMENTS_ID = {id}");
                SqlCommand UpdateCommand = new SqlCommand(sql, conn);
                UpdateCommand.ExecuteNonQuery();
             
            }
        }
        public static void DeleteDepartment(int DEPARTMENTS_ID)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                string sql = String.Format("delete from DEPARTMENTS where DEPARTMENTS_ID ={0}", DEPARTMENTS_ID);
                SqlCommand MyCmd = new SqlCommand(sql,conn);
                MyCmd.ExecuteScalar();
            }
        }

    }
}
