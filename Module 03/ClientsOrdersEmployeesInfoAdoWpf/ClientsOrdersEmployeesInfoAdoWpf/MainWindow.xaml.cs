﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DataSet data = new DataSet();

        public MainWindow()
        {
            InitializeComponent();

            dgDepartments.ItemsSource = DBaccess.GetDepartments(data);
            dgProducts.ItemsSource = DBaccess.GetProducts(data);
            dgEmployees.ItemsSource = DBaccess.GetEmployees(data);
            dgBanks.ItemsSource = DBaccess.GetBanks(data);
            dgClients.ItemsSource = DBaccess.GetClients(data);
        }

        private void btnInsertDepartment_Click(object sender, RoutedEventArgs e)
        {
            Department w = new Department();
            w.Show();
        }

        private void btnUpdateDepartment_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dataGrid = dgDepartments;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell RowColumn = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue_id = ((TextBlock)RowColumn.Content).Text;
            int a = Convert.ToInt32(CellValue_id);

            DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
            string CellValueName = ((TextBlock)RowColumn1.Content).Text;

            DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
            string CellValueRegion = ((TextBlock)RowColumn2.Content).Text;

            Department w = new Department();
            w.tbDepartmentNumber.Text = CellValue_id;
            w.tbDepartmentName.Text = CellValueName;
            w.tbDepartmentLocation.Text = CellValueRegion;
            w.Show();

        }

        private void btnDeleteDepartment_Click(object sender, RoutedEventArgs e)
        {
            DataGridRow row = (DataGridRow)dgDepartments.ItemContainerGenerator.ContainerFromIndex(dgDepartments.SelectedIndex);
            DataGridCell RowColumn = dgDepartments.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue_id = ((TextBlock)RowColumn.Content).Text;
            int a = Convert.ToInt32(CellValue_id);

            DataGridCell RowColumn1 = dgDepartments.Columns[1].GetCellContent(row).Parent as DataGridCell;
            string CellValueName = ((TextBlock)RowColumn1.Content).Text;

            DataGridCell RowColumn2 = dgDepartments.Columns[2].GetCellContent(row).Parent as DataGridCell;
            string CellValueRegion = ((TextBlock)RowColumn2.Content).Text;

            Department w = new Department();
            w.tbDepartmentNumber.Text = CellValue_id;
            w.tbDepartmentName.Text = CellValueName;
            w.tbDepartmentLocation.Text = CellValueRegion;
            w.Show();
        }

        
    }



}

