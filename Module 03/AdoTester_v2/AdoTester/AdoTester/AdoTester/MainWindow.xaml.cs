﻿using System.Windows;
//Ado.Net
using System.Data;

namespace AdoTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DataSet data=new DataSet();
        public MainWindow()
        {
            InitializeComponent();
            Departments.ItemsSource = DBAccess.GetDepartments(data);
            Employees.ItemsSource = DBAccess.GetEmployees(data);
        }
        private void btnAddEmployee_Click(object sender, RoutedEventArgs e)
        {
            //create structure of table
            DataRow newRow = data.Tables["Employees"].NewRow();
            newRow["EmployeesName"] = "Ivanov 4";
            newRow["DepartmantsID"] = "2";
            newRow["Job"] = "-";
            data.Tables["Employees"].Rows.Add(newRow);

            DBAccess.InsertEmployee(data);
        }
    }
}
