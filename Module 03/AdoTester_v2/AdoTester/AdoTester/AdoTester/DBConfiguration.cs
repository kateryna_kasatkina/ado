﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoTester
{
    /// <summary>
    /// Отвечает за работу со строкой соединения: чтение app.config, настройка параметров соединения в строке соединения и т.п.
    /// </summary>
    public class DBConfiguration
    {
        private static string dbConnectionString;
        private static string dbProviderName;
 
        static DBConfiguration()
        {
            dbConnectionString = "Data Source=d:\\MyData\\MyData;";
        }

        public static string DbConnectionString
        {
            get { return dbConnectionString; }
        }

        public static string DbProviderName
        {
            get { return dbProviderName; }
        }

    }
}
