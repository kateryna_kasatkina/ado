﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Ado.Net
using System.Data;
using System.Data.Common;
using System.Data.SQLite;

namespace AdoTester
{
    public class DepartmentsSQLs
    {
        
        public  static string selectAllEmployees()
        {
            return "select * from Employees";
        }

        public static string selectAllDepartments()
        {
            return "select * from Departments";
        }
    }

}
