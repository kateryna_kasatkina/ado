﻿using MyObjects.dll;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    public class MyContext:DbContext
    {
        public MyContext()
            :base("LINQtest")
        { }

        public DbSet<MyAccount> accounts { get; set; }
        public DbSet<MyClient> clients { get; set;}
        public DbSet<MyOrder> orders { get; set; }
        public DbSet<MyOrdersPosition> ordersPosition { get; set; }
        public DbSet<MyProduct> products { get; set; }

    }
}
