﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects.dll
{
   public  class MyProduct: INotifyPropertyChanged
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
