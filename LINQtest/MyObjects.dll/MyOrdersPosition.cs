﻿using System.ComponentModel;

namespace MyObjects.dll
{
    public class MyOrdersPosition: INotifyPropertyChanged
    {
        public int ID { get; set; }
        public MyProduct Product { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }
        public MyOrder Order { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}