﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects.dll
{
    public class MyClient : INotifyPropertyChanged
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public List<MyAccount> Accounts { get; set; }
        public int Age { get; set; }
        public string RegionInfo { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
