﻿using System.ComponentModel;

namespace MyObjects.dll
{
    public class MyAccount: INotifyPropertyChanged
    {
        public int ID { get; set; }
        public MyClient Client { get; set; }
        public double Account { get; set; }
        public string AccountNumber { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}