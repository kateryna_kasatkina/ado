﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects.dll
{
    public class MyOrder : INotifyPropertyChanged
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string OrdersDate { get; set; }
        public double TotalCost { get; set; }
        public List<MyOrdersPosition> MyOrdersPositions { get; set; }
        public MyClient Client { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
