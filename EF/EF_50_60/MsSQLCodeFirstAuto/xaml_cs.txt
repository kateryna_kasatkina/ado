using System.Data.Entity;

public MainWindow()
        {
            InitializeComponent();
            Refresh();
            //testEmployees();
            deleteCustomer("PTRNK");
            addCustomer("PTRNK", "PETRENKO & Co", "Petrenko", "Owner", "Penrenko house", "Kharkov", "Kharkov area", "61001", "Ukraine", "099-099-67-89", "099-099-67-89");
            editCustomer("PTRNK");
            //AddOrder();
            editCustomerInTransaction("PTRNK");
        }


        private void Refresh()
        {
            if (_entities != null)
                _entities.Dispose();
            _entities = new MyDataModel();

        }
        private void deleteCustomer(string customerId)
        {
            Customers customer = _entities.Customers.Find(customerId);
            if (customer != null)
            {
                List<Orders> orders = _entities.Orders.Where(o => o.Customers.CustomerID == customerId).Distinct().ToList();
                if (orders != null && orders.Count > 0)
                {
                    foreach (Orders o in orders)
                    {
                        if (o != null && o.Order_Details != null)
                            o.Order_Details.Clear();
                    }
                    _entities.Orders.RemoveRange(orders);
                }
                _entities.Customers.Remove(customer);
                _entities.SaveChanges();
                //
                customer = _entities.Customers.Find("PTRN2");
                if (customer!=null)
                {
                    _entities.Customers.Remove(customer);
                    _entities.SaveChanges();
                }
            }
        }
        private void addCustomer(
       string CustomerID
      , string CompanyName
      , string ContactName
      , string ContactTitle
      , string Address
      , string City
      , string Region
      , string PostalCode
      , string Country
      , string Phone
      , string Fax)
        {
            Customers newCustomer = new Customers();
            newCustomer.CustomerID = CustomerID;
            newCustomer.CompanyName = CompanyName;
            newCustomer.ContactName = ContactName;
            newCustomer.ContactTitle = ContactTitle;
            newCustomer.Address = Address;
            newCustomer.City = City;
            newCustomer.Region = Region;
            newCustomer.PostalCode = PostalCode;
            newCustomer.Country = Country;
            newCustomer.Phone = Phone;
            newCustomer.Fax = Fax;
            _entities.Customers.Add(newCustomer);
            _entities.SaveChanges();
        }
        private void editCustomer(string customerId)
        {
            Customers customer = _entities.Customers.Where(c => c.CustomerID == customerId).SingleOrDefault();
            customer.ContactName = customer.ContactName.ToUpper();
            _entities.Entry(customer).State = EntityState.Modified;
            _entities.SaveChanges();
        }
        private void editCustomerInTransaction(string customerId)
        {
            using (MyDataModel db = new MyDataModel())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Customers customer1 = db.Customers.FirstOrDefault(c => c.CustomerID == customerId);
                        customer1.ContactName = customer1.ContactName.ToLower();
                        db.Entry(customer1).State = EntityState.Modified;
                        Customers customer2 = new Customers { CustomerID = "PTRN2", ContactName = "�������� 2", CompanyName = "�������� � ��������", Address = customer1.Address, City = customer1.City, ContactTitle = customer1.ContactTitle, Country = customer1.Country, Phone = customer1.Phone, Fax = customer1.Fax, PostalCode = customer1.PostalCode, Region = customer1.Region };
                        db.Customers.Add(customer2);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }
        }
