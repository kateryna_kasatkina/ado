﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;

namespace MsSQLServerCodeFirstAutoWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MyDataModel _entities;
        public MainWindow()
        {
            InitializeComponent();
            Refresh();
            testEmployees();
            deleteCustomer("PTRNK");
            addCustomer("PTRNK", "PETRENKO & Co", "Petrenko", "Owner", "Penrenko house", "Kharkov", "Kharkov area", "61001", "Ukraine", "099-099-67-89", "099-099-67-89");
            editCustomer("PTRNK");
            AddOrder();
            editCustomerInTransaction("PTRNK");
        }
        private void AddOrder()
        {
            //Создаем Master
            //Создаем к нему нужное колличестов элементов его коллекции
            //мастер добавляется последним (редактируется последним)
            int product_id = 2;
            int employeeId = 5;
            string customerId = "PTRNK";
            Products product = _entities.Products.Where(p => p.ProductID == product_id).FirstOrDefault();
            Customers customer = _entities.Customers.Find(customerId);
            Employees employee = _entities.Employees.Find(employeeId);
            Orders newOrder = new Orders();
            newOrder.CustomerID = customer.CustomerID;
            newOrder.Customers = customer;
            newOrder.EmployeeID = employee.EmployeeID;
            newOrder.Employees = employee;
            newOrder.OrderDate = DateTime.Now;
            newOrder.ShipAddress = customer.Address;
            newOrder.ShippedDate = DateTime.Now.AddDays(7);
            Order_Details ordersLine = new Order_Details();
            ordersLine.Products = product;
            ordersLine.ProductID = product.ProductID;
            ordersLine.UnitPrice = new Decimal(1250.87);
            ordersLine.Quantity = 2;
            ordersLine.Orders = newOrder;
            ordersLine.OrderID = newOrder.OrderID;
            newOrder.Order_Details.Add(ordersLine);
            _entities.Orders.Add(newOrder);
            _entities.SaveChanges();
        }

        private void testEmployees()
        {
            _entities.Employees.Load();
            List<Employees> emp = new List<Employees>();
            foreach (Employees e in _entities.Employees)
            {
                emp.Add(e);
            }
            this.dGrid1.ItemsSource = emp;
        }
        private void Refresh()
        {
            if (_entities != null)
                _entities.Dispose();
            _entities = new MyDataModel();

        }
        private void deleteCustomer(string customerId)
        {
            Customers customer = _entities.Customers.Find(customerId);
            if (customer != null)
            {
                List<Orders> orders = _entities.Orders.Where(o => o.Customers.CustomerID == customerId).Distinct().ToList();
                if (orders != null && orders.Count > 0)
                {
                    foreach (Orders o in orders)
                    {
                        if (o != null && o.Order_Details != null)
                            o.Order_Details.Clear();
                    }
                    _entities.Orders.RemoveRange(orders);
                }
                _entities.Customers.Remove(customer);
                _entities.SaveChanges();
                //
                customer = _entities.Customers.Find("PTRN2");
                if (customer!=null)
                {
                    _entities.Customers.Remove(customer);
                    _entities.SaveChanges();
                }
            }
        }
        private void addCustomer(
       string CustomerID
      , string CompanyName
      , string ContactName
      , string ContactTitle
      , string Address
      , string City
      , string Region
      , string PostalCode
      , string Country
      , string Phone
      , string Fax)
        {
            Customers newCustomer = new Customers();
            newCustomer.CustomerID = CustomerID;
            newCustomer.CompanyName = CompanyName;
            newCustomer.ContactName = ContactName;
            newCustomer.ContactTitle = ContactTitle;
            newCustomer.Address = Address;
            newCustomer.City = City;
            newCustomer.Region = Region;
            newCustomer.PostalCode = PostalCode;
            newCustomer.Country = Country;
            newCustomer.Phone = Phone;
            newCustomer.Fax = Fax;
            _entities.Customers.Add(newCustomer);
            _entities.SaveChanges();
        }
        private void editCustomer(string customerId)
        {
            Customers customer = _entities.Customers.Where(c => c.CustomerID == customerId).SingleOrDefault();
            customer.ContactName = customer.ContactName.ToUpper();
            _entities.Entry(customer).State = EntityState.Modified;
            _entities.SaveChanges();
        }
        private void editCustomerInTransaction(string customerId)
        {
            using (MyDataModel db = new MyDataModel())
            {
                //db.Database.Connection
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Customers customer1 = db.Customers.FirstOrDefault(c => c.CustomerID == customerId);
                        customer1.ContactName = customer1.ContactName.ToLower();
                        db.Entry(customer1).State = EntityState.Modified;
                        Customers customer2 = new Customers { CustomerID = "PTRN2", ContactName = "Петренко 2", CompanyName = "Петренок и Товарищи", Address = customer1.Address, City = customer1.City, ContactTitle = customer1.ContactTitle, Country = customer1.Country, Phone = customer1.Phone, Fax = customer1.Fax, PostalCode = customer1.PostalCode, Region = customer1.Region };
                        db.Customers.Add(customer2);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

    }
}
