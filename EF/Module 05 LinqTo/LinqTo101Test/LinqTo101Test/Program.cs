﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTo101Test
{
    class Program
    {
        class Product
        {
            public string Name { get; set; }
            public int CategoryID { get; set; }
        }

        class Category
        {
            public string Name { get; set; }
            public int ID { get; set; }
        }

        static void Main(string[] args)
        {
            // Specify the first data source.
            List<Category> categories = new List<Category>()
                 {
                     new Category(){Name="Beverages", ID=001},
                     new Category(){ Name="Condiments", ID=002},
                     new Category(){ Name="Vegetables", ID=003},
                     new Category() {ID=004 }
                 };

            // Specify the second data source.
            List<Product> products = new List<Product>()
                {
                   new Product{Name="Tea",  CategoryID=001},
                   new Product{Name="Mustard", CategoryID=002},
                   new Product{Name="Pickles", CategoryID=002},
                   new Product{Name="Carrots", CategoryID=003},
                   new Product{Name="Bok Choy", CategoryID=003},
                   new Product{Name="Peaches", CategoryID=005},
                   new Product{Name="Melons", CategoryID=005},
                   new Product{Name="Ice Cream", CategoryID=007},
                   new Product{Name="Mackerel", CategoryID=012},
                   new Product { Name="Soap"},
                   new Product {Name="Mask" }
                 };


            /*var supplierCusts =
                    from sup in suppliers
                    join cust in customers on sup.Country equals cust.Country into cs
                    from c in cs.DefaultIfEmpty()  // DefaultIfEmpty preserves left-hand elements that have no matches on the right side 
                    orderby sup.SupplierName
                    select new
                    {
                        Country = sup.Country,
                        CompanyName = c == null ? "(No customers)" : c.CompanyName,
                        SupplierName = sup.SupplierName
                    };*/

            //1. Вывести все  продукты и их категории
            var query = from x in products
                        join cat in categories on x.CategoryID equals cat.ID
                        from c in cat.DefaultIfEmpty()
                        select new { ProductName = x.Name, ProductCaategory = cat.Name };
            foreach (var item in query)
            {
                Console.WriteLine($"{item.ProductName},{item.ProductCaategory}");
            }

            Console.WriteLine("=================================================");
            //2. Вывести продукты только из указанной категории
            string str = "Vegetables";
            var query1 = from cat in categories
                         where cat.Name == str
                         join prod in products on cat.ID equals prod.CategoryID
                         select new { ProductName = prod.Name, ProductCaategory = cat.Name };
            foreach (var item in query1)
            {
                Console.WriteLine($"{item.ProductName},{item.ProductCaategory}");
            }

            Console.WriteLine("=================================================");
            // 3.Вывести только те продукты, для которых задана категория
            var query2 = from  prod in products
                         where prod.CategoryID != 0
                         select new { ProductName = prod.Name, ProductCategory = prod.CategoryID};
            foreach (var item in query2)
            {
                Console.WriteLine($"{item.ProductName},{item.ProductCategory}");
            }




            Console.WriteLine("=================================================");
            //4.Вывести только те продукты, для которых не задана категория
            var query3 = from prod in products
                         where prod.CategoryID == 0
                         select new { ProductName = prod.Name, ProductCategory = prod.CategoryID };
            foreach (var item in query3)
            {
                Console.WriteLine($"{item.ProductName},{item.ProductCategory}");
            }
        }
    }
}

