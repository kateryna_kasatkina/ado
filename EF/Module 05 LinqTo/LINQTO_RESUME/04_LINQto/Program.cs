﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_LINQto
{
    class Program
    {
       
        #region Classes
        public class Employee
        {
            public string LastName { get; set; }
            public string FirstName { get; set; }
        }
        #endregion

        static void Main(string[] args)
        {
            #region Data
            var employees = new List<Employee>
            {
                new Employee {LastName = "Ivanov", FirstName = "Ivan"},
                new Employee {LastName = "Andreev", FirstName = "Andrew"},
                new Employee {LastName = "Petrov", FirstName = "Petr"}
            };
            #endregion


            var query = from emp in employees
                        let fullName = emp.FirstName + " " + emp.LastName // let - новый локальный идентификатор.
                        orderby fullName descending
                        select fullName;

            foreach (var person in query)
                Console.WriteLine(person);

            var query1 = from x in Enumerable.Range(0, 10)
                        let innerRange = Enumerable.Range(0, 10)
                        from y in innerRange
                        select new { X = x, Y = y, Product = x * y };

            foreach (var item in query1)
            {
                Console.WriteLine("{0}*{1}={2}",item.X,item.Y,item.Product);
               // Console.WriteLine($"{item.X}*{item.Y}={item.Product}");
            }
            Console.ReadKey();
        }
        
    }
}
