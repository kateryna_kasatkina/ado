﻿using System.Collections.Generic;
using System.Data.Entity;

namespace RelationsManyToMany
{
    #region Entities
    public class Vendor
    {
        public Vendor()
        {
            VendorProducts = new List<Product>();
        }

        public virtual int VendorId
        {
            get;
            set;
        }
        public virtual string VendorName
        {
            get;
            set;
        }
        public virtual ICollection<Product> VendorProducts
        {
            get;
            set;
        }
    }
    public class Product
    {
        public Product()
        {
            ProductVendors = new List<Vendor>();
        }
        public virtual int ProductId
        {
            get;
            set;
        }
        public virtual string ProductName
        {
            get;
            set;
        }
        public virtual ICollection<Vendor> ProductVendors
        {
            get;
            set;
        }
    }
    #endregion


    public class SuperMarketContext : DbContext
    {
        public SuperMarketContext():base("name = ConnString")
        {

        }
        public DbSet<Product> Product
        {
            get;
            set;
        }
        public DbSet<Vendor> Vendor
        {
            get;
            set;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Vendor>()
        .HasMany(v => v.VendorProducts)
        .WithMany(p => p.ProductVendors)
        .Map(
        m =>
        {
            m.MapLeftKey("VendorId");
            m.MapRightKey("ProductId");
            m.ToTable("VendorProduct");
        });

        }

    }



    #region




    #endregion
}
