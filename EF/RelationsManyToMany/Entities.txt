
    #region Entities
    public class Vendor
    {
        public Vendor()
        {
            VendorProducts = new List<Product>();
        }

        public virtual int VendorId
        {
            get;
            set;
        }
        public virtual string VendorName
        {
            get;
            set;
        }
        public virtual ICollection<Product> VendorProducts
        {
            get;
            set;
        }
    }
    public class Product
    {
        public Product()
        {
            ProductVendors = new List<Vendor>();
        }
        public virtual int ProductId
        {
            get;
            set;
        }
        public virtual string ProductName
        {
            get;
            set;
        }
        public virtual ICollection<Vendor> ProductVendors
        {
            get;
            set;
        }
    }
    #endregion
