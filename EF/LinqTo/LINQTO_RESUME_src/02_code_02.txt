int[] numbers = { 1, 1, 2, 3,5,8,13,21,34 };

            var query = from x in numbers
                        where x % 2 == 0
                        select x * 2;

            foreach (var item in query)
                Console.WriteLine(item);

            // Delay.
            Console.ReadKey();