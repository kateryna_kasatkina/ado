﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MantyToManyCodeFirstSample
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SuperMArketContext db = new SuperMArketContext())
            {
                Product P1 = new Product();
                P1.ProductName = "Dell products";

                Vendor V1 = new Vendor();
                V1.VendorName = "Vivek";
                V1.VendorProducts.Add(P1);
                db.Vendor.Add(V1);
                db.SaveChanges();
                Console.Write("Product-Vendor test has completed...");
                //1) Create one member and two comments of this member:
                var member1 = new Member { FirstName = "Pete" };
                var comment1 = new Comment { Message = "Good morning!" };
                var comment2 = new Comment { Message = "Good evening!" };
                var memberComment1 = new MemberComment
                {
                    Member = member1,
                    Comment = comment1,
                    Something = 101
                };
                var memberComment2 = new MemberComment
                {
                    Member = member1,
                    Comment = comment2,
                    Something = 102
                };

                db.MemberComments.Add(memberComment1); // will also add member1 and comment1
                db.MemberComments.Add(memberComment2); // will also add comment2

                db.SaveChanges();
                //2) Add a third comment of member1:
                member1 = db.Members.Where(m => m.FirstName == "Pete").SingleOrDefault();
                if (member1 != null)
                {
                    var comment3 = new Comment { Message = "Good night!" };
                    var memberComment3 = new MemberComment
                    {
                        Member = member1,
                        Comment = comment3,
                        Something = 103
                    };

                    db.MemberComments.Add(memberComment3); // will also add comment3
                    db.SaveChanges();
                }
                //3) Create new member and relate it to the existing comment2:
                comment2 = db.Comments.Where(c => c.Message == "Good evening!").SingleOrDefault();
                if (comment2 != null)
                {
                    var member2 = new Member { FirstName = "Paul" };
                    var memberComment4 = new MemberComment
                    {
                        Member = member2,
                        Comment = comment2,
                        Something = 201
                    };

                    db.MemberComments.Add(memberComment4);
                    db.SaveChanges();
                }
                //4) Create relationship between existing member2 and comment3:
                var Member2 = db.Members.Where(m => m.FirstName == "Paul").SingleOrDefault();
                var Comment3 = db.Comments.Where(c => c.Message == "Good night!").SingleOrDefault();
                if (Member2 != null && Comment3 != null)
                {
                    var memberComment5 = new MemberComment
                    {
                        Member = Member2,
                        Comment = Comment3,
                        Something = 202
                    };

                    db.MemberComments.Add(memberComment5);
                    db.SaveChanges();
                }
                //5)  Delete this relationship again:
                var MemberComment5 = db.MemberComments.Where(mc => mc.Member.FirstName == "Paul" && mc.Comment.Message == "Good night!").SingleOrDefault();
                if (MemberComment5 != null)
                {
                    db.MemberComments.Remove(MemberComment5);
                    db.SaveChanges();
                }
                //6) Delete member1 and all its relationsships to the comments:
                var Member1 = db.Members.Where(m => m.FirstName == "Pete").SingleOrDefault();
                if (Member1 != null)
                {
                    db.Members.Remove(Member1);
                    db.SaveChanges();
                }
                //
                UserTest.DoSomeTest(db);
                //
                Console.Write("Press any key to continue...");
                Console.ReadKey();
            }  
        }
    }
}
