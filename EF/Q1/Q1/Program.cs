﻿using Q1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Q1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*--6. выбрать всех клиентов, у которых задан Регион
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Region
FROM [dbo].[Customers] AS Customers
WHERE Customers.Region IS NOT NULL


*/

            /*using (var context = new AdventureWorksLT2012Entities())
            {
               // var query = from c in context.Customers select c;
                IQueryable<Customer> query = from c in context.Customers select c;
                Console.WriteLine(query);
                Console.WriteLine("Press any key...");
                Console.ReadKey();
                //SELECT ... FROM [SalesLT].[Customer] AS [Extent1]
                foreach (var customer in query)
                {
                    Console.WriteLine(customer.CustomerID);
                }
                Console.WriteLine("Press any key...");
                Console.ReadKey();
                // ... SELECT COUNT(1) AS [A1] FROM [SalesLT].[Customer] AS [Extent1]
                int customersCount = query.Count();

                Console.WriteLine("Total number of records:{0}", customersCount);
            }*/

            using (var context = new NORTHWINDEntities())
            {
                IQueryable<Customers> query = from c in context.Customers select c;

                //Customer firstCustomer = query.First(); //SELECT TOP (1) ... FROM [SalesLT].[Customer] AS [c] (Look @ profiler)
                Customers firstCustomer = query.ToList().First();
                Console.WriteLine("First Customer: {0} {1}", firstCustomer.ContactName, firstCustomer.ContactTitle);

                Console.WriteLine("==============================================================");
                Console.WriteLine(query);
            }
        }
    }
}
