﻿using System;

namespace CF.Data
{
    public class Attendee
    {
        public int AttendeeTrackingID { get; set; }
        public string LastName { get; set; }
        public DateTime? DateAdded { get; set; }
    }
    // become entity due to flient IPI , flient IPI  is analog of attributes data anotation
}
