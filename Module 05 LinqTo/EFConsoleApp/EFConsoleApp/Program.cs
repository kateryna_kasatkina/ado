﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyDataModel _entities = new MyDataModel();
            _entities.Employees.Load();
            List<Employees> emp = new List<Employees>();
            foreach (Employees item in _entities.Employees)
            {
                emp.Add(item);
            }

            foreach (Employees item in _entities.Employees)
            {
                Console.WriteLine(item);
            }
        }
    }
}
