﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SecondLINQ
{
    public partial class MainForm : Form
    {
        private List<Department> Departments = new List<Department>();
        private List<Employee> Employees = new List<Employee>();
        private Dictionary<Department, IEnumerable<Employee>> GroupEmployee;

        public MainForm()
        {
            InitializeComponent();
            dgvEmployees.AutoGenerateColumns = true;
            dgvDepartments.AutoGenerateColumns = true;
            loadDepartments();
            loadEmployees();
            #region LinqTo Test
            var matchingEmployees = null;
            #region
            this.GroupEmployee = matchingEmployees.ToDictionary(x => x.department, y => y.employees);
            bsDepartments.DataSource = GroupEmployee.Keys;
        }

        public class Department
        {
            private int deptNo = 0;
            private String deptName = String.Empty;
            private String deptLoc = String.Empty;

            public Department() { }

            public Department(int DeptNo, String DeptName, String DeptLoc)
            {
                deptNo = DeptNo;
                deptName = DeptName;
                deptLoc = DeptLoc;
            }

            public int DeptNo
            {
                set { deptNo = value; }
                get {return deptNo;}
            }

            public String DeptName
            {
                set { deptName = value; }
                get { return deptName; }
            }

            public String DeptLoc
            {
                set { deptLoc = value; }
                get { return deptLoc; }
            }


        }
        private class Employee
        {
            private int employeeNo;
            private String employeeName;
            private int deptNo;
            private String job;
            private Double salary;

            public Employee() { }

            public Employee(int EmployeeNo, String EmployeeName, int DeptNo, String Job, Double Salary)
            {
                employeeNo = EmployeeNo;
                employeeName = EmployeeName;
                deptNo = DeptNo;
                job = Job;
                salary = Salary;
            }
            public int EmployeeNo
            {
                set { employeeNo = value; }
                get { return employeeNo; }
            }
            public String EmployeeName
            {
                set { employeeName = value; }
                get { return employeeName; }
            }
            public int DeptNo
            {
                set { deptNo = value; }
                get { return deptNo; }
            }

            public String Job
            {
                set { job = value; }
                get { return job; }
            }

            public Double Salary
            {
                set { salary = value; }
                get { return salary; }
            }
        }
        private void loadDepartments()
        {
            Departments.Add(new Department(10, "Sales", "Hyderabad"));
            Departments.Add(new Department(20, "Purchases", "Mumbai"));
            Departments.Add(new Department(30, "Admin", "Mumbai"));
            Departments.Add(new Department(40, "Accounts", "Mumbai"));
            Departments.Add(new Department(50, "Training", "Hyderabad"));
            Departments.Add(new Department(60, "Stores", "Hyderabad"));
        }
        private void loadEmployees()
        {
            Employees.Add(new Employee(1, "Krishna Prasad", 10, "Manager", 30000));
            Employees.Add(new Employee(2, "Rajesh", 10, "Clerk", 10000));
            Employees.Add(new Employee(3, "Ramesh", 10, "Assistant", 5000));
            Employees.Add(new Employee(4, "Ragesh", 10, "Computer Operator", 4000));
            Employees.Add(new Employee(5, "Murali", 20, "Manager", 30000));
            Employees.Add(new Employee(6, "Anil", 20, "Clerk", 10000));
            Employees.Add(new Employee(7, "Karthik", 30, "Manager", 30000));
            Employees.Add(new Employee(8, "Anirudh", 30, "Assistant", 7000));
            Employees.Add(new Employee(9, "Sarma Chada", 40, "Manager", 15000));
            Employees.Add(new Employee(10, "Anupama", 40, "Assistant", 3000));
            Employees.Add(new Employee(10, "Anirudh", 40, "Accountant", 7000));
            Employees.Add(new Employee(11, "Sailesh", 60, "Store Keeper", 4000));
        }
        private void bsDepartments_CurrentChanged(object sender, EventArgs e)
        {
            bsEmployees.DataSource = this.GroupEmployee [(Department) bsDepartments.Current];
        }

    }
}

