﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_LINQto
{
    public static class MySet
    {
        public static IEnumerable<T> Where<T>(this IEnumerable<T> sourse,Func<T,bool> predicate)
        {
            Console.WriteLine("Our realization of Where called");
            return Enumerable.Where(sourse, predicate);
        }

        public static IEnumerable<R> Select<T,R>(this IEnumerable<T>sourse,Func<T,R> selector)
        {
            Console.WriteLine("Our realization of Select called");
            return Enumerable.Select(sourse,selector);
        }
    }
}
