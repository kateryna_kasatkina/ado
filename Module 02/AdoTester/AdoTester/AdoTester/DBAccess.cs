﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Драйвер
using System.Data.SQLite;
using System.Diagnostics;

namespace AdoTester
{
    public class DBAccess
    {
        public static DataView GetDepartments()
        {
            //DataSet - отсоединенный режим работы в Ado.Net
            DataSet data;//"аналог" базы данных
            data = new DataSet();
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
            #region Создание DataSet вручную
            /////*
            ////
            //// Структуры таблиц в базе данных
            ////
            //DataTable table = new DataTable("Departments");
            //table.Columns.Add("DepartmentsID", typeof(int));//
            //table.Columns.Add("DepartmentsName", typeof(string));//
            //table.Columns.Add("DepartmentsLocation", typeof(string));//
            ////первичные ключи: и другие ограничения целостности
            //DataColumn[] keys = new DataColumn[1];//если ключ составной
            //                                      //на два и более столбцов,
            //                                      //то размер массива другой - DataColumn[3];
            //keys[0] = table.Columns["DepartmentsID"];
            ////keys[1] = table.Columns["RegionsID"];
            //table.PrimaryKey = keys;//первичные ключи - это массив
            //table.Columns["DepartmentsName"].AllowDBNull = false;
            //table.Columns["DepartmentsName"].Unique = true;
            //table.Columns["DepartmentsName"].Caption = "Название Отдела";
            ////
            ////добавление данных (совпадаем с реальной базой данных)
            ////
            //table.Rows.Add(1, "Head Office", "New York");
            //table.Rows.Add(2, "IT Dep", "Kharkiv");
            //table.Rows.Add(3, "Accoun Dep", "London");
            //data.Tables.Add(table);
            ////*/
            #endregion
            #region Заполнение Data из базы данных
            ///*
            var connectionString = DBConfiguration.DbConnectionString;
                //"Data Source=d:\\MyData\\MyData;";
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter(DepartmentsSQLs.selectAllTxt(),
                                   connection);
            masterDataAdapter.Fill(data, "Departments");
            //*/
            #endregion
            //data.WriteXml("dataset.dat");
            //data.ReadXml("dataset.dat");
            DataView dv = data.Tables["Departments"].DefaultView;
            return dv;
        }
        public static void InsertEmployee(DataSet ds)
        {
            // SQL to insert into customers
            string insert = @"
                INSERT into Employees
                (
                   EmployeesName,
                   DepartmantsID,
                   Job,
                   Salary
                )
                values
                (
                   @EmployeesName,
                   @DepartmantsID,
                   @Job,
                   @Salary
                )
             ";

            //create connection
            SQLiteConnection conn = new SQLiteConnection(DBConfiguration.DbConnectionString);
            SQLiteDataAdapter da = new SQLiteDataAdapter();
            SQLiteCommand insertCmd = new SQLiteCommand(insert, conn);


            //string getID = @"SELECT @@IDENTITY";//для MS SQL Server
        }
    }
}
