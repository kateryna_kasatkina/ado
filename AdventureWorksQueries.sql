USE [AdventureWorks2008]

--1. ������ ����������, �� ������� ���������� ����� �������� ��� � ����� � ������ ����������, �� ������� � ������

--creating two separate queries:
SELECT 
Employee.JobTitle,
PayHistory.PayFrequency
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
WHERE PayHistory.PayFrequency = 1
GROUP BY Employee.JobTitle, PayHistory.PayFrequency

SELECT 
Employee.JobTitle,
PayHistory.PayFrequency
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
WHERE PayHistory.PayFrequency = 2
GROUP BY Employee.JobTitle, PayHistory.PayFrequency

--or creating one ordered result:
SELECT 
Employee.JobTitle,
PayHistory.PayFrequency
FROM [HumanResources].[Employee] AS Employee
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
GROUP BY Employee.JobTitle, PayHistory.PayFrequency
ORDER BY PayHistory.PayFrequency

--2. ������ �������, � ������� �������� ���������� �� ����������, ������������ ��� � �����
SELECT 
Department.Name AS 'Department name',
PayHistory.PayFrequency
FROM [HumanResources].[Department] AS Department
LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
ON Department.DepartmentID = DepartmentHistory.DepartmentID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON DepartmentHistory.BusinessEntityID = PayHistory.BusinessEntityID
WHERE PayHistory.PayFrequency = 1
GROUP BY Department.Name, PayHistory.PayFrequency
ORDER BY Department.Name;

--3. �������� ������ ����������� � ��������� �� �����, ���������, ���������� ����� � ������ �� �������
SELECT 
Person.LastName,
Person.MiddleName,
Person.FirstName,
Employee.JobTitle,
PayHistory.Rate,
PayHistory.PayFrequency
FROM [Person].[Person] AS Person
LEFT JOIN [HumanResources].[Employee] AS Employee
ON Person.BusinessEntityID = Employee.BusinessEntityID
LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
ON Person.BusinessEntityID = PayHistory.BusinessEntityID
ORDER BY Person.LastName