﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace WpfDataGridForMasterDetail
{
    public class Customers
    {
        public DataView GetCustomers()
        {
            return DBAccess.GetCustomers().Tables["Customers"].DefaultView;
        }
    }
    public class Categories
    {
        public DataView GetCategories()
        {
            return DBAccess.GetCategories().Tables["Categories"].DefaultView;
        }
    }
    public class ProductCategory : INotifyPropertyChanged
    {
        private int _currentCategory;
        public int CurrentCategory
        {
            get { return _currentCategory; }
            set
            {
                _currentCategory = value;

                ProductsInCategory = DBAccess.GetProductsInCategory(_currentCategory).Tables["Products"].DefaultView;
                OnPropertyChanged("CurrentCategory");
            }
        }

        private int _currentProduct;
        public int CurrentProduct
        {
            get { return _currentProduct; }
            set
            {
                _currentProduct = value;

                OrdersFromProduct = DBAccess.GetAllOrdersFromProduct(_currentProduct).Tables["Orders"].DefaultView;
                OnPropertyChanged("CurrentProduct");
            }
        }

        private int _currentOrder;
        public int CurrentOrder
        {
            get { return _currentOrder; }
            set
            {
                _currentOrder = value;
                OnPropertyChanged("CurrentOrder");
            }
        }

        private DataView _productsInCategory;
        public DataView ProductsInCategory
        {
            get { return _productsInCategory; }
            private set
            {
                _productsInCategory = value;
                OnPropertyChanged("ProductsInCategory");
            }
        }

        private DataView _ordersFromProduct;
        public DataView OrdersFromProduct
        {
            get { return _ordersFromProduct; }
            private set
            {
                _ordersFromProduct = value;
                OnPropertyChanged("OrdersFromProduct");
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion INotifyPropertyChanged
    }    
}
