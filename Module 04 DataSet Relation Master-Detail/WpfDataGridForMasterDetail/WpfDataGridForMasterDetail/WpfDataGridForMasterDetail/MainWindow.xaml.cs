﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data;
using System.Data.Common;
using System.Data.SQLite;//System.Data.SQLite.dll from c:\Program Files\System.Data.SQLite\2010\bin\
using System.IO;

namespace WpfDataGridForMasterDetail
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataSet data;
        DataView detailDataView;
        public MainWindow()
        {
            InitializeComponent();
            MakeDataSet();
            DataView dv = data.Tables["Departments"].DefaultView;
            //dv.RowFilter = "DepartmentsLocation = 'Kharkiv'";
            //dv.Sort="DepartmentsLocation";

            //dv = new DataView(data.Tables["Departments"], "DepartmentsLocation = 'London'", "DepartmentsLocation", DataViewRowState.CurrentRows);
            //Create new table based on filtered records
            // DataTable newTable = dv.ToTable("BrazilianContactNames", true, new string[] { "ContactName" });

            dgDepartments1.ItemsSource = dv;
            selectFirstRowInDataGrid();
            selectRowInDataGrid();

            //dgEmployees1.ItemsSource = data.Tables["Employees"].DefaultView;
            test_comboBoxColum.ItemsSource = dv;
            test_comboBoxColum.DisplayMemberPath = "DepartmentsName";

            dgDepartments2.ItemsSource = dv;
            // Bind the details data connector to the master data connector,
            // using the DataRelation name to filter the information in the 
            // details table based on the current row in the master table. 
            //detailsBindingSource.DataSource = masterBindingSource;
            //detailsBindingSource.DataMember = "EmployeesDepartments";
            
            //data.Relations["EmployeesDepartments"].
            //dgEmployees1.ItemsSource = ;
            //dgEmployees1.DisplayMemberPath = "EmployeesDepartments";
            
        }
        private void MakeDataSet()
        {
            // Specify a connection string. Replace the given value with a 
            // valid connection string for a Northwind SQL Server sample
            // database accessible to your system.
            var connectionString = "Data Source=d:\\MyData\\MyData;";
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();

            // Create a DataSet.
            data = new DataSet();
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;

            // Add data from the Customers table to the DataSet.
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter("select * from Departments", connection);
            masterDataAdapter.Fill(data, "Departments");

            // Add data from the Orders table to the DataSet.
            SQLiteDataAdapter detailsDataAdapter = new
                SQLiteDataAdapter("select * from Employees", connection);
            detailsDataAdapter.Fill(data, "Employees");

            // Establish a relationship between the two tables.
            DataRelation relation = new DataRelation("EmployeesDepartments",
                data.Tables["Departments"].Columns["DepartmentsID"],
                data.Tables["Employees"].Columns["DepartmantsID"]);
            data.Relations.Add(relation);

            // Bind the master data connector to the Customers table.
            //masterBindingSource.DataSource = data;
            //masterBindingSource.DataMember = "Departments";
        }

        private void selectFirstRowInDataGrid()
        {
            DataGridRow row = (DataGridRow)dgDepartments1.ItemContainerGenerator.ContainerFromIndex(0);
            object item = dgDepartments1.Items[0];
            dgDepartments1.SelectedItem = item;
            dgDepartments1.ScrollIntoView(item);
            if (row != null)
                row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void selectRowInDataGrid()
        {
            /*
           for (int i = 0; i < dataGrid.Items.Count; i++)
{
   DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(i);
   TextBlock cellContent = dataGrid.Columns[0].GetCellContent(row) as TextBlock;
   if (cellContent != null && cellContent.Text.Equals(textBox1.Text))
   {
       object item = dataGrid.Items[i];
       dataGrid.SelectedItem = item;
       dataGrid.ScrollIntoView(item);
       row.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
       break;
   }
}
           */
        }

        

        private void dgDepartments1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dgEmployees1.ItemsSource = curRow.CreateChildView(data.Relations["EmployeesDepartments"]);
            }
            catch(Exception)
            { }
        }

        private void dgEmployees1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           // DataGridRow row = ItemsControl.ContainerFromElement((DataGrid)sender, e.OriginalSource as DependencyObject) as DataGridRow;
            //if (row == null) return;
            
            int rowIndex = dgEmployees1.SelectedIndex;
            DataRowView curRow = (DataRowView)dgEmployees1.SelectedItem;
            DataRow dr = curRow.Row;
            EditEmployeeWindow form = new EditEmployeeWindow(dr,data);
            form.ShowDialog();
        }

        private SolidColorBrush hb = new SolidColorBrush(Colors.Orange);
        private SolidColorBrush nb = new SolidColorBrush(Colors.White);
        private void dgDepartments1_LoadingRow(object sender, DataGridRowEventArgs e)
        {
           // Product product = (Product)e.Row.DataContext;
            try
            {
                DataRowView curRow = (DataRowView)(e.Row.DataContext);
                if (curRow["DepartmentsLocation"].ToString().Contains("Kharkiv"))
                    e.Row.Background = hb;
                else
                    e.Row.Background = nb;
            }
            catch(Exception)
            {

            }
        }

       

        private void dgDepartments2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //DataRowView curRow = (DataRowView)e.AddedItems[0];
            //detailDataView = curRow.CreateChildView(data.Relations["EmployeesDepartments"]);
        }

        private void dgDepartments2_LoadingRowDetails(object sender, DataGridRowDetailsEventArgs e)
        {
            DataRowView curRow = (DataRowView)e.Row.DataContext;
            detailDataView = curRow.CreateChildView(data.Relations["EmployeesDepartments"]);

            DataGrid dgEmployees2 = e.DetailsElement.FindName("dgEmployees2") as DataGrid;
            if (dgEmployees2 != null)
            {
                dgEmployees2.ItemsSource = detailDataView;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DBAccess.UpdateEmployees(data);
        }

       

       
    }

    public class DepartmentsHelper
    {
        DataSet data;
        public DataView GetDepartments()
        {
            var connectionString = "Data Source=d:\\MyData\\MyData;";
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();

            // Create a DataSet.
            data = new DataSet();
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;

            // Add data from the Customers table to the DataSet.
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter("select * from Departments", connection);
            masterDataAdapter.Fill(data, "Departments");

            DataView dv = data.Tables["Departments"].DefaultView;
            return dv;
        }
    }

}
