﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;//System.Data.SQLite.dll from c:\Program Files\System.Data.SQLite\2010\bin\
using System.Data.SqlClient;
using System.Diagnostics;



namespace WpfDataGridForMasterDetail
{
    public class DBAccess
    {
        public static DataSet GetEmployees()
        {
            // query
            string sql = @"
                SELECT *
                FROM Employees
             ";

            // create connection
            SQLiteConnection conn = new SQLiteConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SQLiteDataAdapter da = new SQLiteDataAdapter();
                da.SelectCommand = new SQLiteCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Employees");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static void UpdateEmployees(DataSet ds)
        {
            // SQL to update customers
            string update = @"
                UPDATE Employees   
                SET
                   EmployeesName = @EmployeesName,
                   DepartmantsID = @DepartmantsID,
                   Job = @Job,
                   Salary = @Salary
                 WHERE
                    EmployeesID = @EmployeesID
             ";

            // create connection
            SQLiteConnection conn = new SQLiteConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SQLiteDataAdapter da = new SQLiteDataAdapter();

                // create command
                SQLiteCommand updateCmd = new SQLiteCommand(update, conn);
                //
                // map parameters
                //
                SQLiteParameter parm1 = updateCmd.Parameters.Add("@EmployeesName", DbType.String, 100, "EmployeesName");
                parm1.SourceVersion = DataRowVersion.Current;
                
                SQLiteParameter parm2 = updateCmd.Parameters.Add("@Job", DbType.String, 100, "Job");
                parm2.SourceVersion = DataRowVersion.Current;
                SQLiteParameter parm3 = updateCmd.Parameters.Add("@Salary", DbType.Double, 0, "Salary");
                parm3.SourceVersion = DataRowVersion.Current;
                
                SQLiteParameter parm4 = updateCmd.Parameters.Add("@DepartmantsID", DbType.Int32, 0, "DepartmantsID");
                parm1.SourceVersion = DataRowVersion.Current;

                // EmployeeID
                SQLiteParameter parm = updateCmd.Parameters.Add("@EmployeesID", DbType.Int32, 0, "EmployeesID");
                parm.SourceVersion = DataRowVersion.Current;

                // Update database
                da.UpdateCommand = updateCmd;
                DataSet dsCopy = ds.GetChanges();
                da.Update(dsCopy, "Employees");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }
        }
        public static void InsertEmployee(DataSet ds)
        {
            // SQL to insert into customers
            string insert = @"
                INSERT into Employees
                (
                   LastName,
                   FirstName,
                   Title,
                   BirthDate,
                   HireDate,
                   Address,
                   City,
                   Region,
                   PostalCode,                   
                   Country,
                   HomePhone
                )
                values
                (
                   @LastName,
                   @FirstName,
                   @Title,
                   @BirthDate,
                   @HireDate,
                   @Address,
                   @City,
                   @Region,
                   @PostalCode,                   
                   @Country,
                   @HomePhone
                )
             ";

            string getID = @"SELECT @@IDENTITY";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();

                // create commands
                SqlCommand insertCmd = new SqlCommand(insert, conn);
                SqlCommand getIDCmd = new SqlCommand(getID, conn);

                //
                // map parameters
                //
                insertCmd.Parameters.Add("@LastName", SqlDbType.NVarChar, 20, "LastName");
                insertCmd.Parameters.Add("@FirstName", SqlDbType.NVarChar, 10, "FirstName");
                insertCmd.Parameters.Add("@Title", SqlDbType.NVarChar, 30, "Title");
                insertCmd.Parameters.Add("@BirthDate", SqlDbType.DateTime, 20, "BirthDate");
                insertCmd.Parameters.Add("@HireDate", SqlDbType.DateTime, 20, "HireDate");
                insertCmd.Parameters.Add("@Address", SqlDbType.NVarChar, 60, "Address");
                insertCmd.Parameters.Add("@City", SqlDbType.NVarChar, 15, "City");
                insertCmd.Parameters.Add("@Region", SqlDbType.NVarChar, 15, "Region");
                insertCmd.Parameters.Add("@PostalCode", SqlDbType.NVarChar, 10, "PostalCode");
                insertCmd.Parameters.Add("@Country", SqlDbType.NVarChar, 15, "Country");
                insertCmd.Parameters.Add("@HomePhone", SqlDbType.NVarChar, 24, "HomePhone");

                // Update database
                da.InsertCommand = insertCmd;
                da.SelectCommand = getIDCmd;
                da.Update(ds, "Employees");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }
        }
        public static DataSet GetCustomers()
        {
            // query
            string sql = @"
                SELECT *
                FROM Customers
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Customers");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static void UpdateCustomers(DataSet ds)
        {
            // SQL to update customers
            string update = @"
                UPDATE Customers   
                SET
                   CompanyName = @CompanyName,
                   ContactName = @ContactName,
                   ContactTitle = @ContactTitle,
                   Address = @address,                
                   City = @city,
                   Region = @region,
                   PostalCode = @postalcode,
                   Country = @country,
                   Phone = @phone,
                   Fax = @Fax
                WHERE
                    CustomerID = @customerid
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();

                // create command
                SqlCommand updateCmd = new SqlCommand(update, conn);

                //
                // map parameters
                //
                updateCmd.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 40, "CompanyName");
                updateCmd.Parameters.Add("@ContactName", SqlDbType.NVarChar, 30, "ContactName");
                updateCmd.Parameters.Add("@ContactTitle", SqlDbType.NVarChar, 30, "ContactTitle");
                updateCmd.Parameters.Add("@address", SqlDbType.NVarChar, 60, "Address");
                updateCmd.Parameters.Add("@city", SqlDbType.NVarChar, 15, "City");
                updateCmd.Parameters.Add("@region", SqlDbType.NVarChar, 15, "Region");
                updateCmd.Parameters.Add("@postalcode", SqlDbType.NVarChar, 10, "PostalCode");
                updateCmd.Parameters.Add("@country", SqlDbType.NVarChar, 15, "Country");
                updateCmd.Parameters.Add("@phone", SqlDbType.NVarChar, 24, "Phone");
                updateCmd.Parameters.Add("@Fax", SqlDbType.NVarChar, 24, "Fax");

                // CustomerID
                SqlParameter parm = updateCmd.Parameters.Add("@customerid", SqlDbType.NChar, 5, "CustomerID");
                parm.SourceVersion = DataRowVersion.Original;

                // Update database
                da.UpdateCommand = updateCmd;
                da.Update(ds, "customers");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }
        }
        public static void InsertCustomer(DataSet ds)
        {
            // SQL to insert into customers
            string insert = @"
                INSERT into Customers
                (
                   CustomerID,
                   CompanyName,
                   ContactName,
                   ContactTitle,
                   Address,
                   City,
                   Region,
                   PostalCode,                   
                   Country,
                   Phone,
                   Fax
                )
                values
                (
                   @CustomerID,
                   @CompanyName,
                   @ContactName,
                   @ContactTitle,
                   @Address,
                   @City,
                   @Region,
                   @PostalCode,                   
                   @Country,
                   @Phone,
                   @Fax
                )
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();

                // create command
                SqlCommand insertCmd = new SqlCommand(insert, conn);

                //
                // map parameters
                //
                insertCmd.Parameters.Add("@CustomerID", SqlDbType.NChar, 5, "CustomerID");
                insertCmd.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 40, "CompanyName");
                insertCmd.Parameters.Add("@ContactName", SqlDbType.NVarChar, 30, "ContactName");
                insertCmd.Parameters.Add("@ContactTitle", SqlDbType.NVarChar, 30, "ContactTitle");
                insertCmd.Parameters.Add("@Address", SqlDbType.NVarChar, 60, "Address");
                insertCmd.Parameters.Add("@City", SqlDbType.NVarChar, 15, "City");
                insertCmd.Parameters.Add("@Region", SqlDbType.NVarChar, 15, "Region");
                insertCmd.Parameters.Add("@PostalCode", SqlDbType.NVarChar, 10, "PostalCode");
                insertCmd.Parameters.Add("@Country", SqlDbType.NVarChar, 15, "Country");
                insertCmd.Parameters.Add("@Phone", SqlDbType.NVarChar, 24, "Phone");
                insertCmd.Parameters.Add("@Fax", SqlDbType.NVarChar, 24, "Fax");

                // Update database
                da.InsertCommand = insertCmd;
                da.Update(ds, "Customers");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }
        }
        public static DataSet GetOrders()
        {
            // query
            string sql = @"
                SELECT *
                FROM Orders
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Orders");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static DataSet GetOrderDetails()
        {
            // query
            string sql = @"
                SELECT *
                FROM OrderDetails
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "OrderDetails");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static DataSet GetCategories()
        {
            // query
            string sql = @"
                SELECT *
                FROM Categories
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Categories");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static DataSet GetProducts()
        {
            // query
            string sql = @"
                SELECT *
                FROM Products
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Products");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static DataSet GetProductsInCategory(int categoryId)
        {
            // query
            string sql = @"
                SELECT *
                FROM Products
                WHERE CategoryID = @categoryId
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);
                da.SelectCommand.Parameters.Add("@categoryId", SqlDbType.Int);
                da.SelectCommand.Parameters["@categoryId"].Value = categoryId;

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Products");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
        public static DataSet GetAllOrdersFromProduct(int productId)
        {
            // query
            string sql = @"
                SELECT Orders.OrderID, Orders.CustomerID, Orders.ShipName, [Order Details].ProductID
                FROM [Order Details] INNER JOIN Orders
                ON Orders.OrderID = [Order Details].OrderID
                WHERE [Order Details].ProductID = @productId
             ";

            // create connection
            SqlConnection conn = new SqlConnection(DBConfiguration.DbConnectionString);

            try
            {
                // Create data adapter
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = new SqlCommand(sql, conn);
                da.SelectCommand.Parameters.Add("@productId", SqlDbType.Int);
                da.SelectCommand.Parameters["@productId"].Value = productId;

                // create and fill dataset
                DataSet ds = new DataSet();
                da.Fill(ds, "Orders");

                return ds;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }

            return null;
        }
    
    }
}
