﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDepartmentsCampanySelectTest
{
    public class Employee
    {
        public string Name { get; set; }
        public decimal Salary { get; set; }
    }

   public class Department
    {
        public string Name { get; set; }

        List<Employee> employees = new List<Employee>();

        public IList<Employee> Employees
        {
            get { return employees; }
        }
    }

   public class Company
    {
        public string Name { get; set; }

        List<Department> departments = new List<Department>();

        public IList<Department> Departments
        {
            get { return departments; }
        }
    }

}
