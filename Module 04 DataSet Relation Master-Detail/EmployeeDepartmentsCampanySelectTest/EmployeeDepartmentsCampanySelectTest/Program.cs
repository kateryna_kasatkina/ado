﻿using System;
using System.Linq;

namespace EmployeeDepartmentsCampanySelectTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var company = new Company
            {
                Name = "A",
                Departments =
                {
                    new Department
                    {
                        Name = "Development",
                        Employees =
                        {
                            new Employee { Name = "T", Salary = 75000m },
                            new Employee { Name = "D", Salary = 45000m },
                            new Employee { Name = "M", Salary = 150000m }
                        }
                    },
                    new Department
                    {
                        Name = "Marketing",
                        Employees =
                        {
                            new Employee { Name = "Z", Salary = 200000m },
                            new Employee { Name = "X", Salary = 120000m }
                        }
                    }
                }
            };


            //after word new created anonimus class with properties Name and Properties
            var query = company.Departments
                                .Select(dept => new { dept.Name, Cost = dept.Employees.Sum(person => person.Salary) })
                                .OrderByDescending(deptWithCost => deptWithCost.Cost);

            var query1 = company.Departments
                               .Select(dept => new int { average_sum  = dept.Employees.Sum(person => person.Salary) })
                               .OrderByDescending(deptWithCost => deptWithCost.Cost);

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }
    }
}
