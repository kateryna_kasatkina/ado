﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClassLibrary
{
    [Table("TeamPlayer")]
    public class Player : Person
    {
        [Index] // Automatically named 'IX_TeamPlayer_Number' 
        public int Number { get; set; }

        public virtual Team Team { get; set; }

        public Player() { }
        public Player(string fn,string ln,string street,string city,int number)
        {
            FirstName = fn;
            LastName = ln;
            Street = street;
            City = city;
            Number = number;
        }
    }
}
