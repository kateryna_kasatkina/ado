﻿namespace ClassLibrary
{
    public class Coach : Person
    {
        public virtual Team Team { get; set; }
    }
}
