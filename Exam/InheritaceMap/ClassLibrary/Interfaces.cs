﻿using System;

namespace ClassLibrary
{
    interface IEntity { Guid Id { get; set; } }
    public interface IHistory
    {
        int Id { get; set; }
        string Hash { get; set; }
        string Context { get; set; }
        DateTime CreateDate { get; set; }
    }
}
