﻿using ClassLibrary;
using System.Data.Entity;

namespace InheritanceConsoleTest
{
    public class AppContext:DbContext
    {
        public AppContext() : base("name=DBforEXAM"){}
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Stadion> Stadions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Coach>()
            .Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Coaches");
            })
            .HasKey(c => c.Id);

            modelBuilder.Entity<Player>()
            .Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Players");
            })
             .HasKey(c => c.Id);

            modelBuilder.Entity<Team>()
                .HasMany(c => c.Players)
                .WithRequired(c => c.Team);

            modelBuilder.Entity<Team>()
            .HasOptional(t => t.Coach)
            .WithOptionalDependent(t => t.Team);


            modelBuilder.Entity<Team>()
                .HasOptional(t => t.Stadion);

          
        }

       
    }
}
