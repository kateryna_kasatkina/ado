namespace InheritanceConsoleTest.Migrations
{
    using ClassLibrary;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<InheritanceConsoleTest.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(InheritanceConsoleTest.AppContext context)
        {
            context.Teams.AddOrUpdate(new Team
            {
                Name = "BestTeam",
                Coach = new Coach { FirstName = "firstNameC", LastName = "LastNameC", Street = "StreetC", City = "City1" },
                Players = new List<Player>
                {
                    new Player { FirstName = "firstName1", LastName = "LastName1", Street = "Street1", City = "City1", Number = 1 },
                    new Player { FirstName = "firstName2", LastName = "LastName2", Street = "Street2", City = "City2", Number = 2 },
                    new Player { FirstName = "firstName3", LastName = "LastName3", Street = "Street3", City = "City1", Number = 3 },
                    new Player { FirstName = "firstName4", LastName = "LastName4", Street = "Street4", City = "City2", Number = 4 },
                    new Player { FirstName = "firstName5", LastName = "LastName5", Street = "Street5", City = "City1", Number = 5 },
                    new Player { FirstName = "firstName6", LastName = "LastName6", Street = "Street6", City = "City1", Number = 6 },
                    new Player { FirstName = "firstName7", LastName = "LastName7", Street = "Street7", City = "City1", Number = 7 },
                    new Player { FirstName = "firstName8", LastName = "LastName8", Street = "Street8", City = "City1", Number = 8 }
                },
                Stadion = new Stadion { Name = "Stadion", City = "City", Street = "Street" }
            });
        }
    }
}
