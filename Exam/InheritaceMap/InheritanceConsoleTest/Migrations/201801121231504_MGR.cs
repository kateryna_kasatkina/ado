namespace InheritanceConsoleTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MGR : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Coaches",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Street = c.String(maxLength: 100),
                        City = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Coach_Id = c.Guid(),
                        Stadion_Name = c.String(maxLength: 128),
                        Stadion_Street = c.String(maxLength: 128),
                        Stadion_City = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Coaches", t => t.Coach_Id)
                .ForeignKey("dbo.Stadions", t => new { t.Stadion_Name, t.Stadion_Street, t.Stadion_City })
                .Index(t => t.Coach_Id)
                .Index(t => new { t.Stadion_Name, t.Stadion_Street, t.Stadion_City });
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Street = c.String(maxLength: 100),
                        City = c.String(nullable: false),
                        Team_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.Team_Id, cascadeDelete: true)
                .Index(t => t.Number)
                .Index(t => t.Team_Id);
            
            CreateTable(
                "dbo.Stadions",
                c => new
                    {
                        Name = c.String(nullable: false, maxLength: 128),
                        Street = c.String(nullable: false, maxLength: 128),
                        City = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Name, t.Street, t.City })
                .Index(t => new { t.Street, t.Name }, name: "IX_Stadion_Main");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teams", new[] { "Stadion_Name", "Stadion_Street", "Stadion_City" }, "dbo.Stadions");
            DropForeignKey("dbo.Players", "Team_Id", "dbo.Teams");
            DropForeignKey("dbo.Teams", "Coach_Id", "dbo.Coaches");
            DropIndex("dbo.Stadions", "IX_Stadion_Main");
            DropIndex("dbo.Players", new[] { "Team_Id" });
            DropIndex("dbo.Players", new[] { "Number" });
            DropIndex("dbo.Teams", new[] { "Stadion_Name", "Stadion_Street", "Stadion_City" });
            DropIndex("dbo.Teams", new[] { "Coach_Id" });
            DropTable("dbo.Stadions");
            DropTable("dbo.Players");
            DropTable("dbo.Teams");
            DropTable("dbo.Coaches");
        }
    }
}
