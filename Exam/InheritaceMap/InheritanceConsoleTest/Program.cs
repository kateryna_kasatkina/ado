﻿using System;
using System.Linq;

namespace InheritanceConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AppContext context = new AppContext())
            {
                var query = context.Players.Where(p => p.Street == "Street2");
                Console.WriteLine(query);
                Console.WriteLine("===========================================");
                foreach (var item in query)
                {
                    Console.WriteLine($"{item.FirstName},{item.LastName}");
                }

                Console.WriteLine("===========================================");
                var query1 = context.Teams.FirstOrDefault();
                foreach (var item in query1.Players)
                {
                    Console.WriteLine($"{item.FirstName},{item.LastName}");
                }
            }
        }
    }
}
