namespace DLL_ADOtest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLOYEES_TO_GROUPS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EMPLOYEES_TO_GROUPS_ID { get; set; }

        public int? GROUPS_ID { get; set; }

        public int? EMPLOYEES_ID { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        public virtual EMPLOYEES EMPLOYEES { get; set; }

        public virtual GROUPS GROUPS { get; set; }
    }
}
