namespace DLL_ADOtest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDERS_POSITIONS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ORDERS_POSITIONS_ID { get; set; }

        public int? ORDERS_ID { get; set; }

        public int? PRODUCTS_ID { get; set; }

        public float? PRICE { get; set; }

        public int? ITEM_COUNT { get; set; }

        public virtual ORDERS ORDERS { get; set; }

        public virtual PRODUCTS PRODUCTS { get; set; }
    }
}
