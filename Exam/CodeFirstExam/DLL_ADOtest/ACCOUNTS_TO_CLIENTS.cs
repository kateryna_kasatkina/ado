namespace DLL_ADOtest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACCOUNTS_TO_CLIENTS
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ACCOUNTS_TO_CLIENTS_ID { get; set; }

        public int? ACCOUNTS_ID { get; set; }

        public int? CLIENTS_ID { get; set; }

        public int? INDICATION { get; set; }

        public virtual ACCOUNTS ACCOUNTS { get; set; }

        public virtual CLIENTS CLIENTS { get; set; }
    }
}
