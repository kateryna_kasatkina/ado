﻿using DbAccess;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DataSet data = new DataSet();
        public static DbProvider provider = DbProvider.GetInstance("Sqlite");

        public MainWindow()
        {
            InitializeComponent();

            //dgDepartments.ItemsSource = DBaccess.GetObjects(data, "DEPARTMENTS");
            //dgProducts.ItemsSource = DBaccess.GetObjects(data, "PRODUCTS");
            //dgEmployees.ItemsSource = DBaccess.GetObjects(data, "EMPLOYEES");
            //dgBanks.ItemsSource = DBaccess.GetObjects(data, "BANKS");
            //dgClients.ItemsSource = DBaccess.GetObjects(data, "CLIENTS");

            dgDepartments.ItemsSource = Data.GetObjects(data, "DEPARTMENTS", provider);
            dgProducts.ItemsSource = Data.GetObjects(data, "PRODUCTS", provider);
            dgEmployees.ItemsSource = Data.GetObjects(data, "EMPLOYEES", provider);
            dgBanks.ItemsSource = Data.GetObjects(data, "BANKS", provider);
            dgClients.ItemsSource = Data.GetObjects(data, "CLIENTS", provider);

        }

        private void btnInsertDepartment_Click(object sender, RoutedEventArgs e)
        {
            Department w = new Department(provider);
            w.Show();
        }

        private void btnUpdateDepartment_Click(object sender, RoutedEventArgs e)
        {
            DataGrid dataGrid = dgDepartments;
            DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
            DataGridCell RowColumn = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue_id = ((TextBlock)RowColumn.Content).Text;
            int a = Convert.ToInt32(CellValue_id);

            DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
            string CellValueName = ((TextBlock)RowColumn1.Content).Text;

            DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
            string CellValueRegion = ((TextBlock)RowColumn2.Content).Text;

            Department w = new Department(provider);
            w.tbDepartmentNumber.Text = CellValue_id;
            w.tbDepartmentName.Text = CellValueName;
            w.tbDepartmentLocation.Text = CellValueRegion;
            w.Show();

        }

        private void btnDeleteDepartment_Click(object sender, RoutedEventArgs e)
        {
            DataGridRow row = (DataGridRow)dgDepartments.ItemContainerGenerator.ContainerFromIndex(dgDepartments.SelectedIndex);
            DataGridCell RowColumn = dgDepartments.Columns[0].GetCellContent(row).Parent as DataGridCell;
            string CellValue_id = ((TextBlock)RowColumn.Content).Text;
            int a = Convert.ToInt32(CellValue_id);

            DataGridCell RowColumn1 = dgDepartments.Columns[1].GetCellContent(row).Parent as DataGridCell;
            string CellValueName = ((TextBlock)RowColumn1.Content).Text;

            DataGridCell RowColumn2 = dgDepartments.Columns[2].GetCellContent(row).Parent as DataGridCell;
            string CellValueRegion = ((TextBlock)RowColumn2.Content).Text;

            Department w = new Department(provider);
            w.tbDepartmentNumber.Text = CellValue_id;
            w.tbDepartmentName.Text = CellValueName;
            w.tbDepartmentLocation.Text = CellValueRegion;
            w.Show();
        }

        
    }



}

