﻿using DbAccess;
using System;
using System.Data;
using System.Windows;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for WAddDepartment.xaml
    /// </summary>
    public partial class Department : Window
    {
        MainWindow myWin = (MainWindow)Application.Current.MainWindow;
        private static DbProvider _provider;
        public Department(DbProvider provider)
        {
            _provider = provider;
            InitializeComponent();
        }

        private void Insert_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = myWin.data.Tables["DEPARTMENTS"].NewRow();
            newRow["DEPARTMENTS_NAME"] = tbDepartmentName.Text;
            newRow["REGION_INFO"] = tbDepartmentLocation.Text;
            myWin.data.Tables["DEPARTMENTS"].Rows.Add(newRow);
            //DBaccess.InsertDepartment(myWin.data);
            Data.Insert(myWin.data, "DEPARTMENTS",_provider);
        }

        //private void Update_Click(object sender, RoutedEventArgs e)
        //{
        //    int id = Convert.ToInt32(tbDepartmentNumber.Text, 10);
        //    string name = tbDepartmentName.Text;
        //    string region = tbDepartmentLocation.Text;
        //    DBaccess.UpdateDepartment(id, name, region);
        //    myWin.data.Clear();
        //    myWin.dgDepartments.ItemsSource = null;
        //   // myWin.dgDepartments.ItemsSource = DBaccess.GetDepartments(myWin.data);
        //    myWin.dgDepartments.ItemsSource = DBaccess.GetObjects(myWin.data, "DEPARTMENTS");
        //    this.Close();
        //}

        //private void Delete_Click(object sender, RoutedEventArgs e)
        //{
        //    int n= Convert.ToInt32(tbDepartmentNumber.Text,10);
        //    DBaccess.DeleteDepartment(n);

        //    myWin.data.Clear();
        //    myWin.dgDepartments.ItemsSource=null;
        //  //  myWin.dgDepartments.ItemsSource= DBaccess.GetDepartments(myWin.data);
        //    myWin.dgDepartments.ItemsSource = DBaccess.GetObjects(myWin.data, "DEPARTMENTS");
        //    this.Close();
        //}
    }
}
