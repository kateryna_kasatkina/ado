﻿using DbAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms
{
    public partial class Departments : Form
    {
        public DataSet data = new DataSet();
        public Departments(DbProvider provider)
        {
            InitializeComponent();
            dgDepartments.DataSource = Data.GetObjects(data, "DEPARTMENTS",provider);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
