﻿namespace ClientsOrdersEmployeesInfoAdoWinForms
{
    partial class Departments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDepartments = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgDepartments)).BeginInit();
            this.SuspendLayout();
            // 
            // dgDepartments
            // 
            this.dgDepartments.AllowUserToAddRows = false;
            this.dgDepartments.AllowUserToDeleteRows = false;
            this.dgDepartments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDepartments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgDepartments.Location = new System.Drawing.Point(0, 0);
            this.dgDepartments.Name = "dgDepartments";
            this.dgDepartments.ReadOnly = true;
            this.dgDepartments.RowTemplate.Height = 24;
            this.dgDepartments.Size = new System.Drawing.Size(539, 546);
            this.dgDepartments.TabIndex = 0;
            this.dgDepartments.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Departments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 546);
            this.Controls.Add(this.dgDepartments);
            this.Name = "Departments";
            this.Text = "Departments";
            ((System.ComponentModel.ISupportInitialize)(this.dgDepartments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgDepartments;
    }
}