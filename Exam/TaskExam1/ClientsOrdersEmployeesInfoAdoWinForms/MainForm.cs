﻿using DbAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms
{
    public partial class Form1 : Form
    {
        public static DbProvider provider = DbProvider.GetInstance("Sql");
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDepartments_Click(object sender, EventArgs e)
        {
            Departments w = new Departments(provider);
            w.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnProducts_Click(object sender, EventArgs e)
        {
            Products w = new Products(provider);
            w.Show();
        }
    }
}
