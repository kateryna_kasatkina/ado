﻿using DbAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms
{
    public partial class Products : Form
    {
        public DataSet data = new DataSet();
        public Products(DbProvider provider)
        {
            InitializeComponent();
            dgProducts.DataSource = Data.GetObjects(data, "PRODUCTS",provider);
        }
    }
}
