﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DbAccess
{
    public class Data
    {
        public static DataView GetObjects(DataSet data, string TableName, DbProvider provider)
        {
            using (var connection = provider.CreateConnection())
            {
                connection.Open();
                string sql = $"select * from {TableName}";
                var adapter = provider.CreateDataAdapter(sql);

                adapter.Fill(data, TableName);
                DataView dv = data.Tables[TableName].DefaultView;
                return dv;
            }
        }


        //public static void InsertDepartment(DataSet data, string TableName, DbProvider provider)
        //{
        //    using (var connection = provider.CreateConnection())
        //    {
        //        connection.Open();
        //        string sql = @"
        //            INSERT into DEPARTMENTS
        //            (
        //               DEPARTMENTS_NAME,
        //               REGION_INFO
        //            )
        //            values
        //            (
        //              @DEPARTMENTS_NAME,
        //              @REGION_INFO
        //            )";
        //        var adapter = provider.CreateDataAdapter(sql);
        //        var command = provider.CreateCommand(sql);

        //        SqlParameter param1 = sqlCom.Parameters.Add("@DEPARTMENTS_NAME", SqlDbType.Char, 4, "DEPARTMENTS_NAME");
        //        param1.SourceVersion = DataRowVersion.Current;

        //        SqlParameter param2 = sqlCom.Parameters.Add("@REGION_INFO", SqlDbType.NChar, 40, "REGION_INFO");
        //        param2.SourceVersion = DataRowVersion.Current;

        //        adapter.InsertCommand = sqlCom;
        //        adapter.Update(data, "DEPARTMENTS");
        //    }
        //}




        //public DataSet ExecuteQuery(string connectionName, string storedProcName, Dictionary<string, DbParameter> procParameters)
        //{
        //    DataSet ds = new DataSet();
        //    using (SqlConnection cn = GetConnection(connectionName))
        //    {
        //        using (SqlCommand cmd = cn.CreateCommand())
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.CommandText = storedProcName;
        //            // assign parameters passed in to the command
        //            foreach (var procParameter in procParameters)
        //            {
        //                cmd.Parameters.Add(procParameter.Value);
        //            }
        //            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
        //            {
        //                da.Fill(ds);
        //            }
        //        }
        //    }
        //    return ds;
        //}



        public static DataView Insert(DataSet data, string TableName, DbProvider provider)
        {

            using (var connection = provider.CreateConnection())
            {
                connection.Open();
                string sql = $"select * from {TableName}";
                var adapter = provider.CreateDataAdapter(sql);
                adapter.Fill(data, TableName);

                var commandBuilder = provider.CreateCommandBuilder();
                commandBuilder.DataAdapter = adapter;

                var insertCommand = commandBuilder.GetInsertCommand();
                var deleteCommand = commandBuilder.GetDeleteCommand();
                adapter.Update(data);


                data.Clear();
                adapter.Fill(data);


                DataView dv = data.Tables[TableName].DefaultView;
                return dv;
            }
           
            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    connection.Open();
            //    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            //    DataSet ds = new DataSet();
            //    adapter.Fill(ds);

            //    DataTable dt = ds.Tables[0];
            //    DataRow newRow = dt.NewRow();
            //    newRow["DEPARTMENTS_NAME"] = "kate department";
            //    newRow["REGION_INFO"] = "Kharkiv";
            //    // dt.Rows.Add(newRow);
            //    dt.Rows.Add(newRow);

            //    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
            //    //Console.WriteLine("============================================");
            //    var insertCommand = commandBuilder.GetInsertCommand();
            //    var deleteCommand = commandBuilder.GetDeleteCommand();
            //    //Console.WriteLine(insertCommand.CommandText);
            //    //Console.WriteLine(deleteCommand.CommandText);
            //    //Console.WriteLine(commandBuilder.GetInsertCommand().ToString());
            //    //Console.WriteLine("============================================");
            //    adapter.Update(ds);


            //    ds.Clear();
            //    adapter.Fill(ds);

            //    //foreach (DataColumn column in dt.Columns)
            //    //    Console.Write("\t{0}", column.ColumnName);
            //    //Console.WriteLine();
            //    //foreach (DataRow row in dt.Rows)
            //    //{
            //    //    var cells = row.ItemArray;
            //    //    foreach (object cell in cells)
            //    //        Console.Write("\t{0}", cell);
            //    //    Console.WriteLine();
            //    //}
            //}
            //Console.Read();
        }

    }

}
