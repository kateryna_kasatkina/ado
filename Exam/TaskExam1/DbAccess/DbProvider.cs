﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace DbAccess
{
    public class DbProvider
    {
        private static DbProvider instance;
        private string _client;
        
        public string connectionString { get; set; }
                                          
        private DbProvider(string client)
        {
            _client = client;

            if (String.Equals(_client, "Sql"))
            {
                connectionString = @"Data Source=DESKTOP-VTMNLJ6;Initial Catalog=DBforTASK;Integrated Security=True";
            }
            if (String.Equals(_client, "Sqlite"))
            {
               // dbConnectionString = "Data Source=d:\\MyData\\MyData;";
               // connectionString = "Data Source=D:\\ADO\\ado\\EF\\EF_50_60\\SQLite3\\MyBD;";
                connectionString = "Data Source = d:\\MyBD;";
            }
        }
        public static DbProvider GetInstance(string client)
        {
            if (instance == null)
            {
                instance = new DbProvider(client);
            }
            return instance;
        }

        public virtual DbConnection CreateConnection()
        {
            if (String.Equals(_client, "Sql"))
            {
                return new SqlConnection(connectionString);
            }
            if (String.Equals(_client, "Sqlite"))
            {
                return new SQLiteConnection(connectionString);
            }
            throw new Exception("string client is uncorrect");
        }
        public virtual DbCommand CreateCommand(string sql)
        {
            if (String.Equals(_client, "Sql"))
            {
                return new SqlCommand(sql);
            }
            if (String.Equals(_client, "Sqlite"))
            {
                return new SQLiteCommand(sql);
            }
            throw new Exception("string client is uncorrect");
        }
        public virtual DbDataAdapter CreateDataAdapter(string sql)
        {
            if (String.Equals(_client, "Sql"))
            {
                return new SqlDataAdapter(sql,connectionString);
            }
            if (String.Equals(_client, "Sqlite"))
            {
                return new SQLiteDataAdapter(sql, connectionString);
            }
            throw new Exception("string client is uncorrect");
        }

        public virtual DbParameter CreateParameter(string sql)
        {
            if (String.Equals(_client, "Sql"))
            {
                return new SqlParameter();
            }
            if (String.Equals(_client, "Sqlite"))
            {
                return new SQLiteParameter();
            }
            throw new Exception("string client is uncorrect");
        }

        public virtual DbCommandBuilder CreateCommandBuilder()
        {
            if (String.Equals(_client, "Sql"))
            {
                return new SqlCommandBuilder();
            }
            if (String.Equals(_client, "Sqlite"))
            {
                return new SQLiteCommandBuilder();
            }
            throw new Exception("string client is uncorrect");
        }
    }




}
