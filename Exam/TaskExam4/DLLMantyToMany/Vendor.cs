﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DLLMantyToMany
{
    [Table("Vendors")]
    public class Vendor
    {
        [Key]
        public int VendorId { get; set; }

        [Required]
        public string VendorName { get; set; }

        [Required]
        [ForeignKey("ProductId")]
        public virtual List<Product> Products { get; set; }

        public Vendor()
        {
            Products = new List<Product>();
        }

        public Vendor(string name)
        {
            VendorName = name;
            Products = new List<Product>();
        }

    }
}
