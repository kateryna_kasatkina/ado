﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DLLMantyToMany
{
    [Table("Products")]
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Required]
        public string ProductName { get; set; }

        [Required]
        [ForeignKey("VendorId")]
        public virtual List<Vendor> Vendors { get; set; }

        public Product()
        {
            Vendors = new List<Vendor>();
        }

        public Product(string name)
        {
            ProductName = name;
            Vendors = new List<Vendor>();
        }

    }
}
