namespace DLLMantyToMany.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorId = c.Int(nullable: false, identity: true),
                        VendorName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.VendorId);
            
            CreateTable(
                "dbo.VendorProducts",
                c => new
                    {
                        Vendor_VendorId = c.Int(nullable: false),
                        Product_ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Vendor_VendorId, t.Product_ProductId })
                .ForeignKey("dbo.Vendors", t => t.Vendor_VendorId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.Product_ProductId, cascadeDelete: true)
                .Index(t => t.Vendor_VendorId)
                .Index(t => t.Product_ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VendorProducts", "Product_ProductId", "dbo.Products");
            DropForeignKey("dbo.VendorProducts", "Vendor_VendorId", "dbo.Vendors");
            DropIndex("dbo.VendorProducts", new[] { "Product_ProductId" });
            DropIndex("dbo.VendorProducts", new[] { "Vendor_VendorId" });
            DropTable("dbo.VendorProducts");
            DropTable("dbo.Vendors");
            DropTable("dbo.Products");
        }
    }
}
