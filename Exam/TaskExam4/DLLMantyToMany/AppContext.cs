﻿using System.Data.Entity;

namespace DLLMantyToMany
{
    public class AppContext : DbContext
    {
        public AppContext() : base("name=ManyToManyDataAnotation") { }
        public DbSet<Vendor> vendors { get; set; }
        public DbSet<Product> products { get; set; }
    }
}

