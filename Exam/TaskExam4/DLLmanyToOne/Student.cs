﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DLLmanyToOne
{
    [Table("Students")]
    public class Student
    {
        [Key]
        public int StudentID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int GradeId { get; set; }

        [Required]
        [ForeignKey("GradeId")]
        public virtual Grade Grade { get; set; }

        public Student() { }

        public Student(string name,Grade gr)
        {
            Name = name;
            Grade = gr;
        }

    }
}
