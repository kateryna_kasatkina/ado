namespace DLLmanyToOne.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DLLmanyToOne.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DLLmanyToOne.AppContext context)
        {
            Grade gr1 = new Grade("good");
            context.grades.AddOrUpdate(gr1);
            Student st1 = new Student("St1",gr1);
            Student st2 = new Student("St2", gr1);
            Student st3 = new Student("St3", gr1);
            Student st4 = new Student("St4", gr1);
            context.students.AddOrUpdate(st1, st2, st3, st4);
        }
    }
}
