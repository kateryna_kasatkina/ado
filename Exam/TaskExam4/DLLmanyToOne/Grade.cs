﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLLmanyToOne
{
    [Table("Grades")]
    public class Grade
    {
        public Grade()
        {
            Students = new List<Student>();
        }

        public Grade(string name)
        {
            GradeName = name;
            Students = new List<Student>();
        }

        [Key]
        public int GradeId { get; set; }

        public string  GradeName { get; set; }

        public string Section { get; set; }

        public virtual List<Student> Students { get; set; }

    }
}
