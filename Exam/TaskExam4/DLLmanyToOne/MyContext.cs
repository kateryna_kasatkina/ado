﻿using System.Data.Entity;

namespace DLLmanyToOne
{
    public class AppContext:DbContext
    {
        public AppContext() : base("name=OneToManyDataAnotation") { }
        public DbSet<Student> students { get; set; }
        public DbSet<Grade> grades { get; set; }
    }
}
