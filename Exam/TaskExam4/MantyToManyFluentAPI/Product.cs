﻿using System.Collections.Generic;

namespace MantyToManyFluentAPI
{
    public class Product
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public virtual List<Vendor> Vendors { get; set; }

        public Product()
        {
            Vendors = new List<Vendor>();
        }

        public Product(string name)
        {
            ProductName = name;
            Vendors = new List<Vendor>();
        }

    }

}
