namespace MantyToManyFluentAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationTest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                    })
                .PrimaryKey(t => t.ProductId);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorId = c.Int(nullable: false, identity: true),
                        VendorName = c.String(),
                    })
                .PrimaryKey(t => t.VendorId);
            
            CreateTable(
                "dbo.VendorProduct",
                c => new
                    {
                        VendorRefId = c.Int(nullable: false),
                        ProductRefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.VendorRefId, t.ProductRefId })
                .ForeignKey("dbo.Vendors", t => t.VendorRefId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductRefId, cascadeDelete: true)
                .Index(t => t.VendorRefId)
                .Index(t => t.ProductRefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VendorProduct", "ProductRefId", "dbo.Products");
            DropForeignKey("dbo.VendorProduct", "VendorRefId", "dbo.Vendors");
            DropIndex("dbo.VendorProduct", new[] { "ProductRefId" });
            DropIndex("dbo.VendorProduct", new[] { "VendorRefId" });
            DropTable("dbo.VendorProduct");
            DropTable("dbo.Vendors");
            DropTable("dbo.Products");
        }
    }
}
