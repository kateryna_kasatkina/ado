// <auto-generated />
namespace MantyToManyFluentAPI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class MigrationTest : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MigrationTest));
        
        string IMigrationMetadata.Id
        {
            get { return "201801101522367_MigrationTest"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
