﻿using System.Data.Entity;

namespace MantyToManyFluentAPI
{
    public class AppContext : DbContext
    {
        public AppContext() : base("name=ManyToManyFluentAPI") { }
        public DbSet<Vendor> vendors { get; set; }
        public DbSet<Product> products { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Vendor>()
                        .HasKey(p=>p.VendorId)
                        .HasMany(s => s.Products)
                        .WithMany(c => c.Vendors)
                        .Map(cs =>
                        {
                            cs.MapLeftKey("VendorRefId");
                            cs.MapRightKey("ProductRefId");
                            cs.ToTable("VendorProduct");
                        });

            modelBuilder.Entity<Product>()
                .HasKey(p => p.ProductId);
                

        }
    }
}
