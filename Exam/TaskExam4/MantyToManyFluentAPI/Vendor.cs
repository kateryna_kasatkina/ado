﻿using System.Collections.Generic;

namespace MantyToManyFluentAPI
{
    public class Vendor
    {
        public int VendorId { get; set; }

        public string VendorName { get; set; }

        public virtual List<Product> Products { get; set; }

        public Vendor()
        {
            Products = new List<Product>();
        }

        public Vendor(string name)
        {
            VendorName = name;
            Products = new List<Product>();
        }

    }
}
