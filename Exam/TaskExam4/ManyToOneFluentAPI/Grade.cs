﻿using System.Collections.Generic;

namespace ManyToOneFluentAPI
{
    public class Grade
    {
        public Grade()
        {
            Students = new List<Student>();
        }

        public Grade(string name)
        {
            GradeName = name;
            Students = new List<Student>();
        }

        public int GradeId { get; set; }

        public string GradeName { get; set; }

        public string Section { get; set; }

        public virtual List<Student> Students { get; set; }

    }
}
