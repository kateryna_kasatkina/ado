﻿namespace ManyToOneFluentAPI
{
    public class Student
    {
        public int StudentID { get; set; }

        public string Name { get; set; }

        public int GradeId { get; set; }

        public virtual Grade Grade { get; set; }

        public Student() { }

        public Student(string name, Grade gr)
        {
            Name = name;
            Grade = gr;
        }

    }
}
