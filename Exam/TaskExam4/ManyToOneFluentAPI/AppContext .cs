﻿using System.Data.Entity;

namespace ManyToOneFluentAPI
{
    class AppContext:DbContext
    {
        public AppContext() : base("name=OneToManyFluentAPI") { }
        public DbSet<Student> students { get; set; }
        public DbSet<Grade> grades { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Grade>()
                .HasKey(p => p.GradeId);
                

            modelBuilder.Entity<Student>()
                .HasKey(p => p.StudentID)
                .HasRequired(s => s.Grade)
                .WithMany(g => g.Students)
                .HasForeignKey(s => s.GradeId)
                .WillCascadeOnDelete();

        }


    }
}

