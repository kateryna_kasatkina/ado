--CREATE DATABASE [DBforTASK];

--use DBforTASK;

--	go
--	CREATE TABLE [dbo].[BANKS](
--	[BANKS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[BANKS_NAME] [varchar](50) NULL,
--	[REGION_INFO] [varchar](50) NULL);

--CREATE TABLE [dbo].[ACCOUNTS](
--	[ACCOUNTS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[DESCRIPTION] [varchar](50) NULL,
--	[BANKS_ID] [int] FOREIGN KEY REFERENCES BANKS(BANKS_ID),
--	[ACCOUNTS_SUM] [money] NULL,
--	[ACCOUNT] [int] NULL);
	
--		go
--	CREATE TABLE [dbo].[CLIENTS](
--	[CLIENTS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[CLIENTS_NAME] [varchar](50) NULL,
--	[CLIENTS_PHONE] [nchar](20) NULL,
--	[CLIENTS_MAIL] [varchar](50) NULL);

--	CREATE TABLE [dbo].[ACCOUNTS_TO_CLIENTS](
--	[ACCOUNTS_TO_CLIENTS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[ACCOUNTS_ID] [int] FOREIGN KEY REFERENCES ACCOUNTS(ACCOUNTS_ID),
--	[CLIENTS_ID] [int] FOREIGN KEY REFERENCES CLIENTS(CLIENTS_ID),
--	[INDICATION] [int] NULL);

--	CREATE TABLE [dbo].[DEPARTMENTS](
--	[DEPARTMENTS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[DEPARTMENTS_NAME] [varchar](50) NULL,
--	[REGION_INFO] [varchar](50) NULL);

--	go
--	CREATE TABLE [dbo].[EMPLOYEES](
--	[EMPLOYEES_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[EMPLOYEES_NAME] [varchar](50) NULL,
--	[PHONE] [varchar](50) NULL,
--	[MAIL] [varchar](50) NULL,
--	[SALARY] [money] NULL,
--	[DEPARTMENTS_ID] [int] NULL);

--	go
--	CREATE TABLE [dbo].[ORDERS](
--	[ORDERS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[DESCRIPTION] [varchar](50) NULL,
--	[ORDERS_DATE] [date] NULL,
--	[TOTAL_COSTS] [money] NULL,
--	[CLIENTS_ID] [int] FOREIGN KEY REFERENCES CLIENTS(CLIENTS_ID));

--	go
--	CREATE TABLE [dbo].[PRODUCTS](
--	[PRODUCTS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[PRODUCTS_NAME] [varchar](50) NULL,
--	[PRICE] [money] NULL);

--	go
--	CREATE TABLE [dbo].[ORDERS_POSITIONS](
--	[ORDERS_POSITIONS_ID] [INT]  IDENTITY(1,1) PRIMARY KEY,
--	[ORDERS_ID] [int] FOREIGN KEY REFERENCES ORDERS(ORDERS_ID),
--	[PRODUCTS_ID] [int] FOREIGN KEY REFERENCES PRODUCTS(PRODUCTS_ID),
--	[PRICE] [money] NULL,
--	[ITEM_COUNT] [int] NULL);


	


insert [dbo].[EMPLOYEES] values(N'employee1',N'pone1',N'mail1@gmail.com',12000,1);
insert [dbo].[EMPLOYEES] values(N'employee2',N'pone2',N'mail2@gmail.com',22000,2);
insert [dbo].[EMPLOYEES] values(N'employee3',N'pone3',N'mail3@gmail.com',32000,3);
insert [dbo].[EMPLOYEES] values(N'employee4',N'pone4',N'mail4@gmail.com',24000,1);
insert [dbo].[EMPLOYEES] values(N'employee5',N'pone6',N'mail6@gmail.com',62000,1);
insert [dbo].[EMPLOYEES] values(N'employee7',N'pone7',N'mail17@gmail.com',72000,3);
select * from EMPLOYEES

insert [dbo].DEPARTMENTS values(N'dep1',N'info1');
insert [dbo].DEPARTMENTS values(N'dep2',N'info2');
insert [dbo].DEPARTMENTS values(N'dep3',N'info3');
insert [dbo].DEPARTMENTS values(N'dep4',N'info4');
insert [dbo].DEPARTMENTS values(N'dep5',N'info5');
select * from DEPARTMENTS


insert [dbo].PRODUCTS (PRODUCTS_NAME)values(N'product1');
insert [dbo].PRODUCTS  values(N'product2',12);
insert [dbo].PRODUCTS  values(N'product3',22);
insert [dbo].PRODUCTS  values(N'product4',33);
insert [dbo].PRODUCTS  values(N'product5',44);
insert [dbo].PRODUCTS  values(N'product6',55);
select * from PRODUCTS


