namespace CodeFirstExam
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class CLIENTS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CLIENTS()
        {
            ACCOUNTS_TO_CLIENTS = new HashSet<ACCOUNTS_TO_CLIENTS>();
            ORDERS = new HashSet<ORDERS>();
        }

        public CLIENTS(string name,string pone,string mail)
        {
            CLIENTS_NAME = name;
            CLIENTS_PHONE = pone;
            CLIENTS_MAIL = mail;
        }

        public CLIENTS(int id,string name, string pone, string mail)
        {
            CLIENTS_ID = id;
            CLIENTS_NAME = name;
            CLIENTS_PHONE = pone;
            CLIENTS_MAIL = mail;
        }

        [Key]
        public int CLIENTS_ID { get; set; }

        [StringLength(50)]
        public string CLIENTS_NAME { get; set; }

        [StringLength(20)]
        public string CLIENTS_PHONE { get; set; }

        [StringLength(50)]
        public string CLIENTS_MAIL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCOUNTS_TO_CLIENTS> ACCOUNTS_TO_CLIENTS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS> ORDERS { get; set; }
    }
}
