namespace CodeFirstExam
{
    using System.Data.Entity;
    // CodeFirst from Database 
    public partial class Model : DbContext
    {
        public Model()
            : base("name=ModelExam")
        {
        }

        public virtual DbSet<ACCOUNTS> ACCOUNTS { get; set; }
        public virtual DbSet<ACCOUNTS_TO_CLIENTS> ACCOUNTS_TO_CLIENTS { get; set; }
        public virtual DbSet<BANKS> BANKS { get; set; }
        public virtual DbSet<CLIENTS> CLIENTS { get; set; }
        public virtual DbSet<DEPARTMENTS> DEPARTMENTS { get; set; }
        public virtual DbSet<EMPLOYEES> EMPLOYEES { get; set; }
        public virtual DbSet<ORDERS> ORDERS { get; set; }
        public virtual DbSet<ORDERS_POSITIONS> ORDERS_POSITIONS { get; set; }
        public virtual DbSet<PRODUCTS> PRODUCTS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACCOUNTS>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ACCOUNTS>()
                .Property(e => e.ACCOUNTS_SUM)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BANKS>()
                .Property(e => e.BANKS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<BANKS>()
                .Property(e => e.REGION_INFO)
                .IsUnicode(false);

            modelBuilder.Entity<CLIENTS>()
                .Property(e => e.CLIENTS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<CLIENTS>()
                .Property(e => e.CLIENTS_PHONE)
                .IsFixedLength();

            modelBuilder.Entity<CLIENTS>()
                .Property(e => e.CLIENTS_MAIL)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTMENTS>()
                .Property(e => e.DEPARTMENTS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTMENTS>()
                .Property(e => e.REGION_INFO)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.EMPLOYEES_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.PHONE)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.MAIL)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.SALARY)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.TOTAL_COSTS)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ORDERS_POSITIONS>()
                .Property(e => e.PRICE)
                .HasPrecision(19, 4);

            modelBuilder.Entity<PRODUCTS>()
                .Property(e => e.PRODUCTS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTS>()
                .Property(e => e.PRICE)
                .HasPrecision(19, 4);
        }
    }
}
