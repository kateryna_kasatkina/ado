namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PRODUCTS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCTS()
        {
            ORDERS_POSITIONS = new HashSet<ORDERS_POSITIONS>();
        }

        [Key]
        public int PRODUCTS_ID { get; set; }

        [StringLength(50)]
        public string PRODUCTS_NAME { get; set; }

        [Column(TypeName = "money")]
        public decimal? PRICE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS_POSITIONS> ORDERS_POSITIONS { get; set; }

        public PRODUCTS(string name,decimal price)
        {
            PRODUCTS_NAME = name;
            PRICE = price;
        }

        public PRODUCTS(int id,string name, decimal price)
        {
            PRODUCTS_ID = id;
            PRODUCTS_NAME = name;
            PRICE = price;
        }
    }
}
