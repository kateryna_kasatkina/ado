namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDERS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDERS()
        {
            ORDERS_POSITIONS = new HashSet<ORDERS_POSITIONS>();
        }

        [Key]
        public int ORDERS_ID { get; set; }

        [StringLength(50)]
        public string DESCRIPTION { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ORDERS_DATE { get; set; }

        [Column(TypeName = "money")]
        public decimal? TOTAL_COSTS { get; set; }

        public int? CLIENTS_ID { get; set; }

        public virtual CLIENTS CLIENTS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS_POSITIONS> ORDERS_POSITIONS { get; set; }


        public ORDERS(string description, decimal total_cost, int client_id)
        {
            DESCRIPTION = description;
            ORDERS_DATE = DateTime.Now;
            TOTAL_COSTS = total_cost;
            CLIENTS_ID = client_id;
        }

        public ORDERS(int id,string description, DateTime date, decimal total_cost, int client_id)
        {
            ORDERS_ID = id;
            DESCRIPTION = description;
            ORDERS_DATE = date;
            TOTAL_COSTS = total_cost;
            CLIENTS_ID = client_id;
        }
    }
}
