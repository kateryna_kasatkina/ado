namespace CodeFirstExam
{
    using System.ComponentModel.DataAnnotations;

    public partial class DEPARTMENTS
    {
        [Key]
        public int DEPARTMENTS_ID { get; set; }

        [StringLength(50)]
        public string DEPARTMENTS_NAME { get; set; }

        [StringLength(50)]
        public string REGION_INFO { get; set; }

        public DEPARTMENTS() { }

        public DEPARTMENTS(string name,string region)
        {
            DEPARTMENTS_NAME = name;
            REGION_INFO = region;
        }

        public DEPARTMENTS(int id,string name, string region)
        {
            DEPARTMENTS_ID = id;
            DEPARTMENTS_NAME = name;
            REGION_INFO = region;
        }
    }
}
