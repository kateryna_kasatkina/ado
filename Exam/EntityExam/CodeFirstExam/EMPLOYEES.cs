namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLOYEES
    {
        [Key]
        public int EMPLOYEES_ID { get; set; }

        [StringLength(50)]
        public string EMPLOYEES_NAME { get; set; }

        [StringLength(50)]
        public string PHONE { get; set; }

        [StringLength(50)]
        public string MAIL { get; set; }

        [Column(TypeName = "money")]
        public decimal? SALARY { get; set; }

        public int? DEPARTMENTS_ID { get; set; }

        public EMPLOYEES() { }

        public EMPLOYEES(string name,string phone,string mail,decimal salary,int dep_id)
        {
            EMPLOYEES_NAME = name;
            PHONE = phone;
            MAIL = mail;
            SALARY = salary;
            DEPARTMENTS_ID = dep_id;
        }

        public EMPLOYEES(int id,string name, string phone, string mail, decimal salary, int dep_id)
        {
            EMPLOYEES_ID = id;
            EMPLOYEES_NAME = name;
            PHONE = phone;
            MAIL = mail;
            SALARY = salary;
            DEPARTMENTS_ID = dep_id;
        }
    }
}
