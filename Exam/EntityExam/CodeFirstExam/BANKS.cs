namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BANKS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BANKS()
        {
            ACCOUNTS = new HashSet<ACCOUNTS>();
        }

        [Key]
        public int BANKS_ID { get; set; }

        [StringLength(50)]
        public string BANKS_NAME { get; set; }

        [StringLength(50)]
        public string REGION_INFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCOUNTS> ACCOUNTS { get; set; }
    }
}
