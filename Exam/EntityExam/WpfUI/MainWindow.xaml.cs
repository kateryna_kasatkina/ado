﻿using AppData;
using CodeFirstExam;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WpfUI.Views;

namespace WpfUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public OrdersW orderW { get; set; }
        public bool IsOrderW { get; set; }

        public EmployeeW employeeW { get; set; }
        public bool IsEmployeeW { get; set; }

        public DepartmentW departmentW { get; set; }
        public bool IsDepartmentW { get; set; }

        public ProductW productW { get; set; }
        public bool IsProductW { get; set; }

        public ClientW clientW { get; set; }
        public bool IsClientW { get; set; }

        public MainWindow()
        {
            IsOrderW = false;
            IsEmployeeW = false;
            IsDepartmentW = false;
            IsProductW = false;
            IsClientW = false;
            InitializeComponent();
            FillClients();
            FillProducts();
            FillEmployees();
            FillDepartments();
            FillOrders();
        }

        public void FillClients()
        {
            using (var context = new AppContext())
            {
                GenericRepository<CLIENTS> clientsData = new GenericRepository<CLIENTS>(context);
                IEnumerable<CLIENTS> clients = clientsData.Get();
                dgClients.ItemsSource = clients;

            }
        }

        public void FillProducts()
        {
            using (var context = new AppContext())
            {
                GenericRepository<PRODUCTS> productsData = new GenericRepository<PRODUCTS>(context);
                IEnumerable<PRODUCTS> products = productsData.Get();
                dgProducts.ItemsSource = products;
            }

        }

        public void FillDepartments()
        {
            using (var context = new AppContext())
            {
                GenericRepository<DEPARTMENTS> departmentsData = new GenericRepository<DEPARTMENTS>(context);
                IEnumerable<DEPARTMENTS> departments = departmentsData.Get();
                dgDepartments.ItemsSource = departments;
            }
        }

        public void FillEmployees()
        {
            using (var context = new AppContext())
            {
                GenericRepository<EMPLOYEES> employeesData = new GenericRepository<EMPLOYEES>(context);
                IEnumerable<EMPLOYEES> employees = employeesData.Get();
                dgEmployees.ItemsSource = employees;
            }
        }

        public void FillOrders()
        {
            using (var context = new AppContext())
            {
                GenericRepository<ORDERS> ordersData = new GenericRepository<ORDERS>(context);
                IEnumerable<ORDERS> orders = ordersData.Get();
                dgOrders.ItemsSource = orders;
            }
        }

        public void dgClients_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (IsClientW == false)
            {
                clientW = new ClientW();
                IsClientW = true;
                clientW.Owner = this;
                clientW.Show();
            }
            clientW.Top = 50;
            clientW.Left = 600;
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid.SelectedIndex == -1)
            {
                new ClientW();
            }
            else
            {
                DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
                DataGridCell RowColumn0 = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn3 = dataGrid.Columns[3].GetCellContent(row).Parent as DataGridCell;
                string id = ((TextBlock)RowColumn0.Content).Text;
                clientW.tboxCLIENTS_ID.Text = id;
                string name = ((TextBlock)RowColumn1.Content).Text;
                clientW.tboxCLIENTS_NAME.Text = name;
                string phone = ((TextBlock)RowColumn2.Content).Text;
                clientW.tboxCLIENTS_PHONE.Text = phone;
                string mail = ((TextBlock)RowColumn3.Content).Text;
                clientW.tboxCLIENTS_MAIL.Text = mail;
            }

        }

        private void dgDepartments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsDepartmentW == false)
            {
                departmentW = new DepartmentW();
                IsDepartmentW = true;
                departmentW.Owner = this;
                departmentW.Show();
            }
            departmentW.Top = 50;
            departmentW.Left = 600;
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid.SelectedIndex == -1)
            {
                new ClientW();
            }
            else
            {
                DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
                DataGridCell RowColumn0 = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
                string id = ((TextBlock)RowColumn0.Content).Text;
                departmentW.tboxDEPARTMENTS_ID.Text = id;
                string name = ((TextBlock)RowColumn1.Content).Text;
                departmentW.tboxDEPARTMENTS_NAME.Text = name;
                string region_info = ((TextBlock)RowColumn2.Content).Text;
                departmentW.tboxREGION_INFO.Text = region_info;
            }

        }

        private void dgProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsProductW == false)
            {
                productW = new ProductW();
                IsProductW = true;
                productW.Owner = this;
                productW.Show();
            }
            productW.Top = 50;
            productW.Left = 600;
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid.SelectedIndex == -1)
            {
                new ClientW();
            }
            else
            {
                DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
                DataGridCell RowColumn0 = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
                string id = ((TextBlock)RowColumn0.Content).Text;
                productW.tboxPRODUCTS_ID.Text = id;
                string name = ((TextBlock)RowColumn1.Content).Text;
                productW.tboxPRODUCTS_NAME.Text = name;
                string price = ((TextBlock)RowColumn2.Content).Text;
                productW.tboxPRICE.Text = price;
            }
        }

        private void dgEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsEmployeeW == false)
            {
                employeeW = new EmployeeW();
                IsEmployeeW = true;
                employeeW.Owner = this;
                employeeW.Show();
            }
            employeeW.Top = 50;
            employeeW.Left = 600;
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid.SelectedIndex == -1)
            {
                new ClientW();
            }
            else
            {
                DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
                DataGridCell RowColumn0 = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn3 = dataGrid.Columns[3].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn4 = dataGrid.Columns[4].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn5 = dataGrid.Columns[5].GetCellContent(row).Parent as DataGridCell;
                string id = ((TextBlock)RowColumn0.Content).Text;
                employeeW.tboxEMPLOYEES_ID.Text = id;
                string name = ((TextBlock)RowColumn1.Content).Text;
                employeeW.tboxEMPLOYEES_NAME.Text = name;
                string phone = ((TextBlock)RowColumn2.Content).Text;
                employeeW.tboxPHONE.Text = phone;
                string mail = ((TextBlock)RowColumn3.Content).Text;
                employeeW.tboxMAIL.Text = mail;
                string salary = ((TextBlock)RowColumn4.Content).Text;
                employeeW.tboxSALARY.Text = salary;
                string depId = ((TextBlock)RowColumn5.Content).Text;
                employeeW.tboxEmpDEPARTMENTS_ID.Text = depId;
            }
        }

        private void dgOrders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsOrderW == false)
            {
                orderW = new OrdersW();
                IsOrderW = true;
                orderW.Owner = this;
                orderW.Show();
            }

            orderW.Top = 50;
            orderW.Left = 700;
            DataGrid dataGrid = sender as DataGrid;
            if (dataGrid.SelectedIndex == -1)
            {
                new ClientW();
            }
            else
            {
                DataGridRow row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(dataGrid.SelectedIndex);
                DataGridCell RowColumn0 = dataGrid.Columns[0].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn1 = dataGrid.Columns[1].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn2 = dataGrid.Columns[2].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn3 = dataGrid.Columns[3].GetCellContent(row).Parent as DataGridCell;
                DataGridCell RowColumn4 = dataGrid.Columns[4].GetCellContent(row).Parent as DataGridCell;

                string str0 = ((TextBlock)RowColumn0.Content).Text;
                orderW.tboxORDERS_ID.Text = str0;

                string str1 = ((TextBlock)RowColumn1.Content).Text;
                orderW.tboxDESCRIPTION.Text = str1;

                string str2 = ((TextBlock)RowColumn2.Content).Text;
                orderW.tboxORDERS_DATE.Text = str2;

                string str3 = ((TextBlock)RowColumn3.Content).Text;
                orderW.tboxTOTAL_COSTS.Text = str3;

                string str4 = ((TextBlock)RowColumn4.Content).Text;
                orderW.tboxOrdersCLIENTS_ID.Text = str4;
            }
        }

        private void btnNewClient_Click(object sender, RoutedEventArgs e)
        {
            ClientW clientW = new ClientW();
            clientW.btnDeleteClients.IsEnabled = false;
            clientW.btnUpdateClients.IsEnabled = false;
            IsClientW = true;
            clientW.Show();

        }

        private void btnNewOrder_Click(object sender, RoutedEventArgs e)
        {
            OrdersW w = new OrdersW();
            w.btnDeleteOrder.IsEnabled = false;
            w.btnUpdateOrder.IsEnabled = false;
            IsOrderW = true;
            w.Show();
        }

        private void btnNewEmployee_Click(object sender, RoutedEventArgs e)
        {
            EmployeeW w = new EmployeeW();
            w.btnDeleteEmployee.IsEnabled = false;
            w.btnUpdateEmployee.IsEnabled = false;
            IsEmployeeW = true;
            w.Show();
        }

        private void btnNewProduct_Click(object sender, RoutedEventArgs e)
        {
            ProductW w = new ProductW();
            w.btnDeleteProduct.IsEnabled = false;
            w.btnUpdateProduct.IsEnabled = false;
            IsProductW = true;
            w.Show();
        }

        private void btnNewDepartment_Click(object sender, RoutedEventArgs e)
        {
            DepartmentW w = new DepartmentW();
            w.btnDeleteDepartment.IsEnabled = false;
            w.btnUpdateDepartment.IsEnabled = false;
            IsDepartmentW = true;
            w.Show();
        }
    }
}
