﻿using AppData;
using CodeFirstExam;
using System;
using System.Windows;

namespace WpfUI.Views
{
    /// <summary>
    /// Interaction logic for ProductW.xaml
    /// </summary>
    public partial class ProductW : Window
    {
        MainWindow myWin = (MainWindow)Application.Current.MainWindow;
        public ProductW()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            myWin.IsProductW = false;
        }

        private void btnInsertProduct_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxPRODUCTS_ID.Text != null)
                {
                    PRODUCTS newProduct = new PRODUCTS(tboxPRODUCTS_NAME.Text,Convert.ToDecimal(tboxPRICE.Text));
                    GenericRepository<PRODUCTS> productsData = new GenericRepository<PRODUCTS>(context);
                    productsData.Create(newProduct);
                    myWin.dgProducts.ItemsSource = productsData.Get();
                    myWin.IsProductW = false;
                    this.Close();
                }
            }
        }

        private void btnUpdateProduct_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxPRODUCTS_ID.Text != null)
                {
                    PRODUCTS Product = new PRODUCTS(Convert.ToInt32(tboxPRODUCTS_ID.Text),tboxPRODUCTS_NAME.Text, Convert.ToDecimal(tboxPRICE.Text));
                    GenericRepository<PRODUCTS> productsData = new GenericRepository<PRODUCTS>(context);
                    productsData.Update(Product);
                    myWin.dgProducts.ItemsSource = productsData.Get();
                    myWin.IsProductW = false;
                    this.Close();
                }
            }
        }

        private void UpdateButtonIsEnabledState()
        {
            if (myWin.dgProducts.SelectedItems.Count > 0)
            {
                this.btnDeleteProduct.IsEnabled = true;
            }
            else
                this.btnDeleteProduct.IsEnabled = false;
        }

        private void btnDeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxPRODUCTS_ID.Text != null)
                {
                    int id = Convert.ToInt32(tboxPRODUCTS_ID.Text);
                    GenericRepository<PRODUCTS> productsData = new GenericRepository<PRODUCTS>(context);
                    PRODUCTS product = productsData.FindById(id);
                    productsData.Remove(product);
                    context.SaveChanges();
                    this.UpdateButtonIsEnabledState();
                    myWin.dgProducts.ItemsSource = productsData.Get();
                    myWin.IsProductW = false;
                    this.Close();
                }
            }
        }
    }
}
