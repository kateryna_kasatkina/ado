﻿using AppData;
using CodeFirstExam;
using System;
using System.Windows;

namespace WpfUI.Views
{
    /// <summary>
    /// Interaction logic for Orders.xaml
    /// </summary>
    public partial class OrdersW : Window
    {
        MainWindow myWin = (MainWindow)Application.Current.MainWindow;
        public OrdersW()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            myWin.IsOrderW = false;
        }

        private void btnInsertOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxORDERS_ID.Text != null)
                {
                    ORDERS newOrder = new ORDERS(tboxDESCRIPTION.Text,Convert.ToDecimal(tboxTOTAL_COSTS.Text),Convert.ToInt32(tboxOrdersCLIENTS_ID.Text));
                    GenericRepository<ORDERS> ordersData = new GenericRepository<ORDERS>(context);
                    ordersData.Create(newOrder);
                    myWin.dgOrders.ItemsSource = ordersData.Get();
                    myWin.IsOrderW = false;
                    this.Close();
                }
            }
        }

        private void btnUpdateOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxORDERS_ID.Text != null)
                {
                    ORDERS Order = new ORDERS(Convert.ToInt32(tboxORDERS_ID.Text),tboxDESCRIPTION.Text, Convert.ToDateTime(tboxORDERS_DATE.Text),Convert.ToDecimal(tboxTOTAL_COSTS.Text), Convert.ToInt32(tboxOrdersCLIENTS_ID.Text));
                    GenericRepository<ORDERS> ordersData = new GenericRepository<ORDERS>(context);
                    ordersData.Update(Order);
                    myWin.dgOrders.ItemsSource = ordersData.Get();
                    myWin.IsOrderW = false;
                    this.Close();
                }
            }
        }

        private void UpdateButtonIsEnabledState()
        {
            if (myWin.dgOrders.SelectedItems.Count > 0)
            {
                this.btnDeleteOrder.IsEnabled = true;
            }
            else
                this.btnDeleteOrder.IsEnabled = false;
        }

        private void btnDeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxORDERS_ID.Text != null)
                {
                    int id = Convert.ToInt32(tboxORDERS_ID.Text);
                    GenericRepository<ORDERS> orderData = new GenericRepository<ORDERS>(context);
                    ORDERS order = orderData.FindById(id);
                    orderData.Remove(order);
                    context.SaveChanges();
                    this.UpdateButtonIsEnabledState();
                    myWin.dgOrders.ItemsSource = orderData.Get();
                    myWin.IsOrderW = false;
                    this.Close();
                }
            }
        }
    }
}
