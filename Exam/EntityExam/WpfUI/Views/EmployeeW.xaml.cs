﻿using AppData;
using CodeFirstExam;
using System;
using System.Windows;

namespace WpfUI.Views
{
    /// <summary>
    /// Interaction logic for EmployeeW.xaml
    /// </summary>
    public partial class EmployeeW : Window
    {
        MainWindow myWin = (MainWindow)Application.Current.MainWindow;
        public EmployeeW()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            myWin.IsEmployeeW = false;
        }

        private void btnInsertEmployee_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxEMPLOYEES_ID.Text != null)
                {
                    EMPLOYEES newEmployee = new EMPLOYEES(tboxEMPLOYEES_NAME.Text,tboxPHONE.Text,tboxMAIL.Text, System.Convert.ToDecimal(tboxSALARY.Text), Convert.ToInt32(tboxEmpDEPARTMENTS_ID.Text));
                    GenericRepository<EMPLOYEES> employeesData = new GenericRepository<EMPLOYEES>(context);
                    employeesData.Create(newEmployee);
                    myWin.dgEmployees.ItemsSource = employeesData.Get();
                    myWin.IsEmployeeW = false;
                    this.Close();
                }
            }
        }

        private void UpdateButtonIsEnabledState()
        {
            if (myWin.dgEmployees.SelectedItems.Count > 0)
            {
                this.btnDeleteEmployee.IsEnabled = true;
            }
            else
                this.btnDeleteEmployee.IsEnabled = false;
        }

        private void btnDeleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxEMPLOYEES_ID.Text != null)
                {
                    int id = Convert.ToInt32(tboxEMPLOYEES_ID.Text);
                    GenericRepository<EMPLOYEES> employeesData = new GenericRepository<EMPLOYEES>(context);
                    EMPLOYEES employee = employeesData.FindById(id);
                    employeesData.Remove(employee);
                    context.SaveChanges();
                    this.UpdateButtonIsEnabledState();
                    myWin.dgEmployees.ItemsSource = employeesData.Get();
                    myWin.IsEmployeeW = false;
                    this.Close();
                }
            }
        }

        private void btnUpdateEmployee_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxEMPLOYEES_ID.Text != null)
                {
                    EMPLOYEES Employee = new EMPLOYEES(Convert.ToInt32(tboxEMPLOYEES_ID.Text), tboxEMPLOYEES_NAME.Text, tboxPHONE.Text, tboxMAIL.Text, System.Convert.ToDecimal(tboxSALARY.Text), Convert.ToInt32(tboxEmpDEPARTMENTS_ID.Text));
                    GenericRepository<EMPLOYEES> employeesData = new GenericRepository<EMPLOYEES>(context);
                    employeesData.Update(Employee);
                    myWin.dgEmployees.ItemsSource = employeesData.Get();
                    myWin.IsEmployeeW = false;
                    this.Close();
                }
            }
        }
    }
}
