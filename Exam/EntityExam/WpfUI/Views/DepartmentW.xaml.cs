﻿using AppData;
using CodeFirstExam;
using System;
using System.Windows;

namespace WpfUI.Views
{
    /// <summary>
    /// Interaction logic for DepartmentW.xaml
    /// </summary>
    public partial class DepartmentW : Window
    {
        MainWindow myWin = (MainWindow)Application.Current.MainWindow;
        public DepartmentW()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            myWin.IsDepartmentW = false;
        }

        private void btnInsertDepartment_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxDEPARTMENTS_ID.Text != null)
                {
                    DEPARTMENTS newDepartment = new DEPARTMENTS(tboxDEPARTMENTS_NAME.Text,tboxREGION_INFO.Text);
                    GenericRepository<DEPARTMENTS> departmentsData = new GenericRepository<DEPARTMENTS>(context);
                    departmentsData.Create(newDepartment);
                    myWin.dgDepartments.ItemsSource = departmentsData.Get();
                    myWin.IsDepartmentW = false;
                    this.Close();
                }
            }
        }

        private void btnUpdateDepartment_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxDEPARTMENTS_ID.Text != null)
                {
                    DEPARTMENTS Department = new DEPARTMENTS(Convert.ToInt32(tboxDEPARTMENTS_ID.Text), tboxDEPARTMENTS_NAME.Text, tboxREGION_INFO.Text);
                    GenericRepository<DEPARTMENTS> departmentsData = new GenericRepository<DEPARTMENTS>(context);
                    departmentsData.Update(Department);
                    myWin.dgDepartments.ItemsSource = departmentsData.Get();
                    myWin.IsDepartmentW = false;
                    this.Close();
                }
            }
        }

        private void UpdateButtonIsEnabledState()
        {
            if (myWin.dgDepartments.SelectedItems.Count > 0)
            {
                this.btnDeleteDepartment.IsEnabled = true;
            }
            else
                this.btnDeleteDepartment.IsEnabled = false;
        }

        private void btnDeleteDepartment_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxDEPARTMENTS_ID.Text != null)
                {
                    int id = Convert.ToInt32(tboxDEPARTMENTS_ID.Text);
                    GenericRepository<DEPARTMENTS> departmentsData = new GenericRepository<DEPARTMENTS>(context);
                    DEPARTMENTS department= departmentsData.FindById(id);
                    departmentsData.Remove(department);
                    context.SaveChanges();
                    this.UpdateButtonIsEnabledState();
                    myWin.dgDepartments.ItemsSource = departmentsData.Get();
                    myWin.IsDepartmentW = false;
                    this.Close();
                }
            }
        }
    }
}
