﻿using AppData;
using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Windows;

namespace WpfUI.Views
{
    /// <summary>
    /// Interaction logic for ClientW.xaml
    /// </summary>
    public partial class ClientW : Window
    {
        MainWindow myWin = (MainWindow)Application.Current.MainWindow;
        public ClientW()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            myWin.IsClientW = false;
        }

        private void btnInsertClients_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxCLIENTS_ID.Text != null)
                {
                    CLIENTS newClient = new CLIENTS(tboxCLIENTS_NAME.Text, tboxCLIENTS_PHONE.Text, tboxCLIENTS_MAIL.Text);
                    GenericRepository<CLIENTS> clientsData = new GenericRepository<CLIENTS>(context);
                    clientsData.Create(newClient);
                    myWin.dgClients.ItemsSource = clientsData.Get();
                    myWin.IsClientW = false;
                    this.Close();
                }
            }
        }

        private void btnUpdateClients_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if (tboxCLIENTS_ID.Text != null)
                {
                    CLIENTS Client = new CLIENTS(Convert.ToInt32(tboxCLIENTS_ID.Text),tboxCLIENTS_NAME.Text, tboxCLIENTS_PHONE.Text, tboxCLIENTS_MAIL.Text);
                    GenericRepository<CLIENTS> clientsData = new GenericRepository<CLIENTS>(context);
                    clientsData.Update(Client);
                    myWin.dgClients.ItemsSource = clientsData.Get();
                    myWin.IsClientW = false;
                    this.Close();
                }
            }
        }

      
        private void UpdateButtonIsEnabledState()
        {
            if (myWin.dgClients.SelectedItems.Count > 0)
            {
                this.btnDeleteClients.IsEnabled = true;
            }
            else
                this.btnDeleteClients.IsEnabled = false;
        }

        private void btnDeleteClients_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new AppData.AppContext())
            {
                if(tboxCLIENTS_ID.Text!=null)
                {
                    int id = Convert.ToInt32(tboxCLIENTS_ID.Text);
                    GenericRepository<CLIENTS> clientsData = new GenericRepository<CLIENTS>(context);
                    CLIENTS client = clientsData.FindById(id);
                    clientsData.Remove(client);
                    this.UpdateButtonIsEnabledState();
                    myWin.dgClients.ItemsSource = clientsData.Get();
                    myWin.IsClientW = false;
                    this.Close();
                }
            }
        }
    }
}
