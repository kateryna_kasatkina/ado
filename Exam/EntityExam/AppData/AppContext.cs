﻿using CodeFirstExam;
using System.Data.Entity;

namespace AppData
{
    public class AppContext : DbContext
    {
        public AppContext() : base("name=ModelExam") { }
        public DbSet<ACCOUNTS> accounts { get; set; }
        public DbSet<BANKS> banks { get; set; }
        public DbSet<CLIENTS> clients { get; set; }
        public DbSet<EMPLOYEES> employees { get; set; }
        public DbSet<ORDERS> orders { get; set; }
        public DbSet<DEPARTMENTS> departments { get; set; }
    }
}
