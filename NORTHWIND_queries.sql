USE [NORTHWIND]
--1. ������� ��������, � ������� ���� ������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Orders.OrderID
FROM [dbo].[Customers] AS Customers
JOIN [dbo].[Orders] AS Orders
ON Orders.CustomerID = Customers.CustomerID
WHERE Orders.OrderID IS NOT NULL

--2. ������� ���� ��������, � ������� ��� �� ������ ������
SELECT 
Customers.CustomerID,
Customers.ContactName
FROM [dbo].[Customers] AS Customers
LEFT JOIN [dbo].[Orders] AS Orders
ON Orders.CustomerID = Customers.CustomerID
WHERE Orders.OrderID IS NULL

--3. ������� ��������, � ������� ������ ���� �����
SELECT 
Customers.CustomerID,
Customers.ContactName,
COUNT (Orders.OrderID) AS NUMBER_OF_ORDERS 
FROM [dbo].[Customers] AS Customers
JOIN [dbo].[Orders] AS Orders
ON Orders.CustomerID = Customers.CustomerID
GROUP BY Customers.CustomerID,Customers.ContactName
HAVING COUNT (Orders.OrderID) = 1

--4. ������� ��������, � ������� ����� ���� �������
SELECT 
Customers.CustomerID,
Customers.ContactName,
COUNT (Orders.OrderID) AS NUMBER_OF_ORDERS 
FROM [dbo].[Customers] AS Customers
JOIN [dbo].[Orders] AS Orders
ON Orders.CustomerID = Customers.CustomerID
GROUP BY Customers.CustomerID,Customers.ContactName
HAVING COUNT (Orders.OrderID) > 2

--5. ������� ���� ��������, � ������� �� ����� Fax
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Fax
FROM [dbo].[Customers] AS Customers
WHERE Customers.Fax IS NULL

--6. ������� ���� ��������, � ������� ����� ������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Region
FROM [dbo].[Customers] AS Customers
WHERE Customers.Region IS NOT NULL

--7. ������� ���� ��������, � ������� PostalCode ���������� � ����
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.PostalCode
FROM [dbo].[Customers] AS Customers
WHERE Customers.PostalCode LIKE '0%'

--8. ������� ���� ��������, � ������� PostalCode ������� 5 ��������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.PostalCode
FROM [dbo].[Customers] AS Customers
GROUP BY Customers.CustomerID,Customers.ContactName, Customers.PostalCode
HAVING (LEN(Customers.PostalCode) > 5)


--9. ������� ���� ��������, � ������� PostalCode ���������� � ���� � ������� 5 ��������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.PostalCode
FROM [dbo].[Customers] AS Customers
GROUP BY Customers.CustomerID,Customers.ContactName, Customers.PostalCode
HAVING (LEN(Customers.PostalCode) > 5) AND Customers.PostalCode LIKE '0%'


--10. ������� ���� ��������, � ������� Phone ������������� �� 4
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Phone
FROM [dbo].[Customers] AS Customers
WHERE Customers.Phone LIKE '%4'

--11. ������� ���� ��������, � ������� Phone �������� ������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Phone
FROM [dbo].[Customers] AS Customers
WHERE Customers.Phone LIKE '%(%)%'

--12. ������� ���� ��������, � ������� Phone ��� ������� � ������� � ����� ��� 1 ������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Phone
FROM [dbo].[Customers] AS Customers
WHERE Customers.Phone LIKE '%(_)%'

--13. ������� ���� ��������, � ������� Phone ��� ������� � ������� � ����� ��� 3 �������
SELECT 
Customers.CustomerID,
Customers.ContactName,
Customers.Phone
FROM [dbo].[Customers] AS Customers
WHERE Customers.Phone LIKE '%(___)%'

--14. ������� ���� ��������, � ������� Phone ����� ������� ���
SELECT
Customers.CustomerID,
Customers.ContactName,
Customers.Phone,
LEN(Customers.Phone) AS PHONE_LENGTH
FROM [dbo].[Customers] AS Customers
WHERE LEN(Customers.Phone) = 
(
SELECT
MAX(LEN(Customers.Phone))
FROM [dbo].[Customers] AS Customers
)


--15. ������� ���� ��������, � ������� Phone ����� ������� ��� ���������� � �������
SELECT
Customers.CustomerID,
Customers.ContactName,
Customers.Phone,
LEN(Customers.Phone) AS PHONE_LENGTH
FROM [dbo].[Customers] AS Customers
WHERE Customers.Phone LIKE '%(%)%' AND LEN(Customers.Phone) = 
(
SELECT
MAX(LEN(Customers.Phone))
FROM [dbo].[Customers] AS Customers
WHERE Customers.Phone LIKE '%(%)%'
)

--16. ��������� ���������� �������� � ������ �������
SELECT 
Customers.Region,
COUNT(Customers.CustomerID) AS CUSTOMERS_NUMBER
FROM [dbo].[Customers] AS Customers
GROUP BY Customers.Region
ORDER BY CUSTOMERS_NUMBER DESC


--17. ������� ������ �������� �� ���� �������, � ������� �� ������ �����
SELECT  
Customers.CustomerID,
Customers.ContactName,
Customers.Region
FROM [dbo].[Customers] AS Customers,(
SELECT TOP 1
Customers.Region,
COUNT(Customers.CustomerID) as 'Customers amount'
FROM [dbo].[Customers] AS Customers
GROUP BY Customers.Region
ORDER BY 2 desc
) x
WHERE Customers.Region = x.Region or (Customers.Region is NULL and x.Region is NULL)

--18. ������� ������ ���������� (Employee), ������� ������� � ��������� �� ���� �������, ��� �������� ������ �����
SELECT
Employees.EmployeeID,
Employees.LastName,
Employees.FirstName,
Ter.TerritoryDescription
FROM [dbo].[Employees] AS Employees 
LEFT JOIN [dbo].[EmployeeTerritories] AS EmplTer
ON EmplTer.EmployeeID = Employees.EmployeeID
LEFT JOIN [dbo].[Territories] AS Ter
ON Ter.TerritoryID = EmplTer.TerritoryID
WHERE Ter.TerritoryDescription IN 
(
SELECT
Customers.City
FROM [dbo].[Customers] AS Customers
GROUP BY Customers.City
HAVING (COUNT (Customers.CustomerID))= 
(
SELECT TOP 1
COUNT (Customers.CustomerID)
FROM [dbo].[Customers] AS Customers
GROUP BY Customers.City
ORDER BY 1 ASC
)
)

--18. ������� ������ �������� � ����������, ������� ����������� ���������, � ��������� �����
--������������� �� ����������� ��������� � ������, � ������� ��������� ��������
SELECT
Region.RegionID,
Region.RegionDescription,
Territories.TerritoryID,
Territories.TerritoryDescription,
Employees.LastName,
Employees.FirstName,
Employees.City
FROM [dbo].[Employees] AS Employees
LEFT JOIN [dbo].[EmployeeTerritories] AS ETerritories
ON ETerritories.EmployeeID = Employees.EmployeeID
LEFT JOIN [dbo].[Territories] AS Territories
ON Territories.TerritoryID = ETerritories.TerritoryID
LEFT JOIN [dbo].[Region] AS Region
ON Region.RegionID = Territories.RegionID
