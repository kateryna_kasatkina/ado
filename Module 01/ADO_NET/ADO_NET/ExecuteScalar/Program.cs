﻿using System;
using System.Data.SqlClient;

//Выполнение команд, вовращающие скалярные значения

namespace ExecuteScalar
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source =.\SQLEXPRESS;Initial Catalog=AdventureWorks2014;Integrated Security = True;";
            SqlConnection connection = new SqlConnection(conStr);
            connection.Open(); //открытие подключения

            SqlCommand cmd = new SqlCommand("SELECT PhoneNumber FROM Person.PersonPhone WHERE  BusinessEntityID=1", connection);

            string phoneNumber = (string)cmd.ExecuteScalar(); // выполнение команды

            Console.WriteLine("Phone Number: " + phoneNumber);
        }
    }
}
