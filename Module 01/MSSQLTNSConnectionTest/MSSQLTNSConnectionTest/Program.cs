﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;

namespace MSSQLTNSConnectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            TestDSNOdbcConnection();
        }

        private static void TestDSNOdbcConnection()
        {
            OdbcConnection DbConnection = new OdbcConnection("DSN=MY_DB");
            DbConnection.Open();
            OdbcCommand DbCommand = DbConnection.CreateCommand();
            //DbCommand.CommandText = "SELECT * FROM Person.Person";
            DbCommand.CommandText = "select Person.Person.BusinessEntityID,Person.Person.FirstName from Person.Person";
            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            int fCount = DbReader.FieldCount;
            Console.Write(":");
            for (int i = 0; i < fCount; i++)
            {
                String fName = DbReader.GetName(i);
                //String fName = DbReader[""] (i);
                Console.Write(fName + ":");
            }
            Console.WriteLine();
            while (DbReader.Read())
            {
                Console.Write(":");
                for (int i = 0; i < fCount; i++)
                {
                    String col = DbReader.GetString(i);
                    Console.Write(col + ":");
                }
                Console.WriteLine();
            }

            DbReader.Close();
            DbCommand.Dispose();
            DbConnection.Close();
            Console.WriteLine("Press any...");
            Console.ReadKey();
        }

    }
}
